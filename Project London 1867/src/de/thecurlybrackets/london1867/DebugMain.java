package de.thecurlybrackets.london1867;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import de.thecurlybrackets.londonlib.game.LondonWorld;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.Person;
import de.thecurlybrackets.londonlib.game.worldobjects.places.Place;
import de.thecurlybrackets.londonlib.parser.components.SessionPool;

public class DebugMain {
	
	public static boolean isNotNull(Object obj) {
		return obj != null;
	}
	
	public static void main(String[] args) {
		try {
			SessionPool sessionPool = new SessionPool();
			LondonWorld world = new LondonWorld("tWorld", sessionPool);
			var mod = sessionPool.provideLondonMod(new Label("tMod"), world);
			new Place(new Label("testPlace"), mod);
			var part = sessionPool.provideLondonPart(new Label("tPart"), mod);
			new Person("testPerson", part);
			world.attachComponentCollection(mod);
			
			part.saveContents();
			mod.saveContents();
			world.saveContents();
			System.out.println(sessionPool.generateCurrentStats());
			System.out.println(sessionPool.getPlace(new Label("tMod"), new Label("testPlace")));
			System.out.println(sessionPool.getEntity(new Label("tPart"), new Label("testPerson")));
			System.out.println(sessionPool.getProp(new Label("tPart"), new Label("testProp")));
			
		} catch (IOException | IllegalArgumentException | ParserConfigurationException e) {
			System.out.println(e.getLocalizedMessage());
		}
	}
}
