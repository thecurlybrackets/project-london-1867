# Project "London 1867"
## Description

Initially, this project was meant to be a Steampunk-Game taking place in Victorian London.
The goal, however, has changed to becoming a Text-Adventure-Creator adding an example-world changing the setting
from Victorian London to Prague in 1867. All of this is part of a school project and, due to the defaults, written in
Java ([OpenJDK 12](https://openjdk.java.net/projects/jdk/12/ "OpenJDK 12 - Dedicated Website"))
whereat the GUI is built upon [OpenJFX 12](https://openjfx.io/ "OpenJFX by Gluon - Homepage").

Aside these, the included editor depends on [Jackson](https://github.com/FasterXML/jackson "The Jackson Project").
 
Additionally, there is a [Maven](https://maven.apache.org/ "Apache Maven Project")-Management for these dependencies.
We recommend using either [Eclipse IDE for Java](https://www.eclipse.org/ "Eclipse Foundation") or
[IntelliJ IDEA](https://www.jetbrains.com/idea/?fromMenu "IntelliJ IDEA by JetBrains") as both e.g. have a built-in support for Maven.

Currently, the future extend of this project remains unknown.

## Project Hierarchy:

1. The game-launcher (*Project London 1867*)
	+ A basic environment for the purpose of world-loading and playing
	+ An interface for various kinds of personalization
2. An editor for world-creation (*Project London 1867 Editor*) aka **Londonmaker**
	+ Creation of
		+ Parts
		+ Mods
		+ Worlds
	+ GUI for creation
3. The core library on which both of the obove are based (*Project London 1867 Library*) aka **Londonlib**
	+ Objects (e.g. Entities, Props etc.)
	+ Parsing

## Dependency Structure and Components (Internals):
+ **LondonPart** (.londonpart)
	+ Entities
	+ Props

+ **LondonMod** (.londonmod) loads **LondonPart**s into a **SessionPool**
	+ Places
	+ Quests

+ **LondonWorld** (.londonworld) loads **LondonMod**s into a **SessionPool**
	+ Connects Places to a coherent world
	+ Defines the initial Player
	+ Progress is saved to a *.londonvault*-file

**LondonWorld** depends on **LondonMod** + **LondonPart**