module londonmaker {
	exports de.thecurlybrackets.londonmaker;
	exports de.thecurlybrackets.londonmaker.file;
	exports de.thecurlybrackets.londonmaker.graphics;
	exports de.thecurlybrackets.londonmaker.graphics.dialog;
	exports de.thecurlybrackets.londonmaker.graphics.dialog.types;
	exports de.thecurlybrackets.londonmaker.graphics.javafx;
	exports de.thecurlybrackets.londonmaker.json;
	exports de.thecurlybrackets.londonmaker.json.dataclasses;
	exports de.thecurlybrackets.londonmaker.resource_bundle.language;
	exports de.thecurlybrackets.londonmaker.resource_bundle.settings;
	exports de.thecurlybrackets.londonmaker.tools;
	requires com.fasterxml.jackson.annotation;
	requires com.fasterxml.jackson.core;
	requires com.fasterxml.jackson.databind;
	requires javafx.fxml;
	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.base;
}