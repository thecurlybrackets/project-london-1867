package de.thecurlybrackets.londonmaker.graphics.javafx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;


final class Items {

    public static final String WORLD_FILE = "World-File (*.world)";
    public static final String EDITOR_FILE = "Editor-File (*.londonedit)";

    public static final ObservableList<String> list =
            FXCollections.observableArrayList(WORLD_FILE, EDITOR_FILE);

}

public class ExportController {

    @FXML
    public void initialize() {
        fx_filetype_choicebox.getItems().addAll(Items.list);

        fx_filetype_choicebox.getSelectionModel().selectedIndexProperty().addListener((observable, oldIndex, newIndex) -> {
            switch (((int)newIndex)) {
                case 0:
                    System.out.println(Items.list.get(((int)newIndex)));
                    break;
                case 1:
                    System.out.println(Items.list.get(((int)newIndex)));
            }
        });

    }

    @FXML
    public ChoiceBox<String> fx_filetype_choicebox;

}
