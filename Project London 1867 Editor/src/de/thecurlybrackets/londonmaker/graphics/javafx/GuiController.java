package de.thecurlybrackets.londonmaker.graphics.javafx;


import com.fasterxml.jackson.core.JsonFactory;
import de.thecurlybrackets.londonmaker.file.Extension;
import de.thecurlybrackets.londonmaker.file.File;
import de.thecurlybrackets.londonmaker.graphics.dialog.Dialog;
import de.thecurlybrackets.londonmaker.graphics.dialog.DialogResponse;
import de.thecurlybrackets.londonmaker.graphics.dialog.DialogType;
import de.thecurlybrackets.londonmaker.json.JsonReader;
import de.thecurlybrackets.londonmaker.json.JsonWriter;
import de.thecurlybrackets.londonmaker.json.dataclasses.EditorFile;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class GuiController {

    @FXML
    private AnchorPane map_map_fx_anchorpane;

    //debug

    /*private double tmpX, tmpY;

    @FXML
    public void initialize() {

        Node[] rects = new Node[20];
        for(int i = 0; i < 20; i++){
            rects[i] = new Rectangle(i*30,i%5*30,20,20);
        }
        map_map_fx_anchorpane.getChildren().addAll(rects);

        //debug

        //anchor pane has to be resized
        //forebid resize ab einer bestimmten Größe
        //mehr dynamic durch relativität

        map_map_fx_anchorpane.addEventHandler(ScrollEvent.SCROLL, event -> {

            if(event.getDeltaY() > 0) {
                //scale * 2
                System.out.println(event.getDeltaY());
            } else if(event.getDeltaY() < 0) {
                //scale / 2
                System.out.println(event.getDeltaY());
            }

        });

        map_map_fx_anchorpane.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            tmpX = event.getX();
            tmpY = event.getY();
        });

        map_map_fx_anchorpane.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            double diffX = event.getX() - tmpX;
            double diffY = event.getY() - tmpY;

            for (Node rect : rects) {
                rect.setTranslateX(rect.getTranslateX() + diffX);
                rect.setTranslateY(rect.getTranslateY() + diffY);
            }

            tmpX = event.getX();
            tmpY = event.getY();
        });


        //check if all needed files exist else create and prepare content (eg. first scene-file)

    }*/


    /* internal methods */

    /*private void openNewSubWindow(String windowTitle, String relativeFXMLPath, boolean isResizeable) throws IOException {
        Stage stage = new Stage();

        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(Window.main_stage);
        stage.setTitle(windowTitle);
        stage.setResizable(isResizeable);

        AnchorPane pane = FXMLLoader.load(getClass().getResource(relativeFXMLPath));

        stage.setScene(new Scene(pane));
        stage.showAndWait();
    }*/

    @FXML
    public void menubar_file_new__onActionEvent() {



    }

    @FXML
    public void menubar_file_open__onActionEvent() {

    }

    @FXML
    public void menubar_file_closeFile__onActionEvent() {

    }

    @FXML
    public void menubar_file_save__onActionEvent() {

    }

    @FXML
    public void menubar_file_saveAs__onActionEvent() {

    }


    /* menubar */

    //String path = "D:\\Daten\\Programming\\Java\\London 1867 Git\\editorTest\\edit";

    /*private static String editorFilePath = "";
    private static JsonOrganizer jsonOrganizer;

    public static void setEditorFilePath(String e) {
        editorFilePath = e;
    }

    public static void setJsonOrganizer(JsonOrganizer j) {
       jsonOrganizer = j;
    }


    @FXML
    void menubar_file_new__onActionEvent() throws IOException {
        //fxml-file braucht update
        openNewSubWindow("New World", "NewWorld.fxml", false);
    }

    @FXML
    void menubar_file_open__onActionEvent() throws IOException {

        if(!editorFilePath.equals("")) {

            saveBefore("opening");

            //close loaded content (just if it is better for memory)

        }

        try {
            editorFilePath = PathChooser.openFile
                    ("Open Editor File", null, new FileChooser.ExtensionFilter("London Editor Files", "*.londonedit")).getPath();

            //actually load the data into the window

        } catch (NullPointerException e) {}

        jsonOrganizer = new JsonOrganizer();
        if(!jsonOrganizer.initOpen(editorFilePath)) {
            editorFilePath = "";
        }

    }

    @FXML
    void menubar_file_closeFile__onActionEvent() throws IOException {

        if(editorFilePath.equals("")) {

            new ErrorAlert("No File Closed", "There was no open file that could be closed");

            return;
        }

        saveBefore("closing");

        editorFilePath = "";

        //actually close the window

    }

    @FXML
    void menubar_file_save__onActionEvent() throws IOException {

        if(editorFilePath.equals("")) {

            new ErrorAlert("No File Saved", "There was no open file that could be saved");

            return;
        }

        jsonOrganizer.updateFiles();

    }

    @FXML
    void menubar_file_saveAs__onActionEvent() throws IOException {

        if(editorFilePath.equals("")) {

            new ErrorAlert("No File Saved", "There was no open file that could be saved");

            return;
        }

        try {
            editorFilePath = PathChooser.saveFile("Save Editor-File As", null, new FileChooser.ExtensionFilter("London Editor Files", "*.londonedit")).getPath();
            String segmentDirectoryPath = PathChooser.openDirectory("Save Segments As", null).getPath();

            jsonOrganizer.initSaveAs(editorFilePath, segmentDirectoryPath);
            jsonOrganizer.updateFiles();

            System.out.println("'Save As' worked");

        } catch (NullPointerException e) {}

    }

    private void saveBefore(String typeOfAction) throws IOException {
        ButtonType wantToSave = new QuestionAlert("Saving before " + typeOfAction, "Do you want to save your current progress before " + typeOfAction).buttonType;

        if(wantToSave == ButtonType.YES) {
            jsonOrganizer.updateFiles();
        } else if(wantToSave == ButtonType.CANCEL) {
            return;
        } else {

            ButtonType wantToSave2 = new QuestionAlert("Saving before " + typeOfAction, "Are you sure?").buttonType;

            if(wantToSave2 == ButtonType.NO) {
                jsonOrganizer.updateFiles();
            } else if(wantToSave2 == ButtonType.CANCEL) {
                return;
            }

        }
    }


    //in construction

    //@FXML
    void createNewRoom(ActionEvent event) {
        //wenn noch keine Szene im scenerodner existiert, wird die scene 0,0 erstellt und raum wird der koordinate 0,0 der scene zugefügt

        //sonst
        //checke Koordinate und füge room dort hinzu

        //vllt umgedrehte If für Performance
        //vielleicht eigene Klasse (Zählt Position der Maus)
    }*/


}
