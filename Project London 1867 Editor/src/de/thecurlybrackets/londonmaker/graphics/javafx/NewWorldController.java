package de.thecurlybrackets.londonmaker.graphics.javafx;

import de.thecurlybrackets.londonmaker.graphics.dialog.Dialog;
import de.thecurlybrackets.londonmaker.graphics.dialog.DialogType;
import de.thecurlybrackets.londonmaker.tools.PathChooser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class NewWorldController {

    @FXML
    private TextField world_file_directory__textfield;

    @FXML
    private TextField editor_file_directory__textfield;

    @FXML
    private TextField segment_file_directory__textfield;


    @FXML
    public void world_file_directory_button__onActionEvent(ActionEvent event) {
        File path = PathChooser.openDirectory("Choose World-File Directory", null);

        if(path != null) {
            world_file_directory__textfield.setText(path.getPath());
        }
    }

    @FXML
    public void editor_file_directory_button__onActionEvent(ActionEvent event) {
        File path = PathChooser.openDirectory("Choose Editor-File Directory", null);

        if(path != null) {
            editor_file_directory__textfield.setText(path.getPath());
        }
    }

    @FXML
    public void segment_file_directory_button__onActionEvent(ActionEvent event) {
        File path = PathChooser.openDirectory("Choose Segment-File Directory", null);

        if(path != null) {
            segment_file_directory__textfield.setText(path.getPath());
        }
    }


    @FXML
    public void agree_button__onAction(ActionEvent event) throws IOException {
        if(!new File(world_file_directory__textfield.getText()).exists() ||
                !new File(editor_file_directory__textfield.getText()).exists() ||
                !new File(segment_file_directory__textfield.getText()).exists()) {
            Dialog.create(DialogType.WARNING, "Invalid Path", null, "One of the paths entered is invalid");
            return;
        }

        //GuiController.setEditorFilePath(editor_file_directory__textfield.getText());
        //GuiController.setJsonOrganizer(new JsonOrganizer());

        deny_button__onAction(null);
    }

    @FXML
    public void deny_button__onAction(ActionEvent event) {
        ((Stage)world_file_directory__textfield.getScene().getWindow()).close();
    }


}
