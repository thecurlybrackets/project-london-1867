package de.thecurlybrackets.londonmaker.graphics;

import de.thecurlybrackets.londonmaker.resource_bundle.language.LanguageLoader;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Window extends Application {

    private final String TITLE = LanguageLoader.get("editor_window_title");
    public static Stage main_stage;

    @Override
    public void start(Stage stage) throws Exception {
        main_stage = stage;
        stage.setTitle(TITLE);

        AnchorPane pane = FXMLLoader.load(getClass().getResource("javafx/Gui.fxml"));

        stage.setScene(new Scene(pane));
        stage.show();

        //stage.setMaxWidth(stage.getWidth());
        //stage.setMaxHeight(stage.getHeight());
        stage.setMinWidth(800);
        stage.setMinHeight(600);
    }

    public static void run(String... args) {
        launch(args);
    }

}
