package de.thecurlybrackets.londonmaker.graphics.dialog;

import de.thecurlybrackets.londonmaker.graphics.dialog.types.ChoiceDialog;
import de.thecurlybrackets.londonmaker.graphics.dialog.types.WarningDialog;
import de.thecurlybrackets.londonmaker.graphics.dialog.types.InformationDialog;

public enum DialogType {

    CHOICE(new ChoiceDialog()),
    INFORMATION(new InformationDialog()),
    WARNING(new WarningDialog());

    private Dialog dialog;

    DialogType(Dialog dialog) {
        this.dialog = dialog;
    }

    public Dialog getDialog() {
        return dialog;
    }

}
