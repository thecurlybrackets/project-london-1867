package de.thecurlybrackets.londonmaker.graphics.dialog;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public abstract class Dialog {

    public static DialogResponse create(DialogType type, String title, String headerMessage, String bodyMessage) {

        Dialog dialog = type.getDialog();

        dialog.setTitle(title);
        dialog.setHeaderMessage(headerMessage);
        dialog.setBodyMessage(bodyMessage);
        dialog.generate();

        return dialog.getResponse();

    }

    protected final Alert alert;

    protected Dialog(Alert alert) {
        this.alert = alert;
    }

    private void setTitle(String title) {
        alert.setTitle(title);
    }

    private void setHeaderMessage(String message) {
        alert.setHeaderText(message);
    }

    private void setBodyMessage(String message) {
        alert.setContentText(message);
    }

    protected abstract void generate();

    protected DialogResponse getResponse() {
        return null;
    }

}
