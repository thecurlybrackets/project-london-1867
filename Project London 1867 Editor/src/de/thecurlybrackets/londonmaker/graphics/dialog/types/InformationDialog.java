package de.thecurlybrackets.londonmaker.graphics.dialog.types;

import de.thecurlybrackets.londonmaker.graphics.dialog.Dialog;
import javafx.scene.control.Alert;

public class InformationDialog extends Dialog {

    public InformationDialog() {
        super(new Alert(Alert.AlertType.INFORMATION));
    }

    @Override
    protected void generate() {
        alert.close();
        alert.showAndWait();
    }
}
