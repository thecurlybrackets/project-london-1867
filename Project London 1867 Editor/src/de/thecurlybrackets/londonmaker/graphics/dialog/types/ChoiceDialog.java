package de.thecurlybrackets.londonmaker.graphics.dialog.types;

import de.thecurlybrackets.londonmaker.graphics.dialog.Dialog;
import de.thecurlybrackets.londonmaker.graphics.dialog.DialogResponse;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class ChoiceDialog extends Dialog {

    private Optional<ButtonType> result;

    public ChoiceDialog() {
        super(new Alert(Alert.AlertType.NONE));
    }

    @Override
    protected void generate() {

        alert.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

        alert.close();
        result = alert.showAndWait();
    }

    @Override
    protected DialogResponse getResponse() {

        ButtonType type = result.get();
        if(type == ButtonType.YES) {
            return DialogResponse.YES;
        } else if(type == ButtonType.NO) {
            return DialogResponse.NO;
        } else if(type == ButtonType.CANCEL) {
            return DialogResponse.CANCEL;
        } else {
            return null;
        }

    }

}
