package de.thecurlybrackets.londonmaker.graphics.dialog.types;

import de.thecurlybrackets.londonmaker.graphics.dialog.Dialog;
import javafx.scene.control.Alert;

public class WarningDialog extends Dialog {

    public WarningDialog() {
        super(new Alert(Alert.AlertType.WARNING));
    }

    @Override
    protected void generate() {
        alert.close();
        alert.showAndWait();
    }
}
