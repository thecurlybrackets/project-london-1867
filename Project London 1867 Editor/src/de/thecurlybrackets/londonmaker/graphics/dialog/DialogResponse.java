package de.thecurlybrackets.londonmaker.graphics.dialog;

public enum DialogResponse {

    YES,
    NO,
    CANCEL

}
