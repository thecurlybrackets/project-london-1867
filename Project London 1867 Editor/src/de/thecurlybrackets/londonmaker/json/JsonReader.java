package de.thecurlybrackets.londonmaker.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class JsonReader {

    private JsonFactory jsonFactory;

    private boolean parseError = false;
    private boolean noFileError = false;

    public JsonReader() {
        this.jsonFactory = new JsonFactory();
    }

    public JsonReader(JsonFactory jsonFactory) {
        this.jsonFactory = jsonFactory;
    }

    public boolean failed() {
        return parseError || noFileError;
    }

    public boolean parseError() {
        return parseError;
    }

    public boolean noFileError() {
        return noFileError;
    }

    public <T> T get(String path, Class<T> cls) {
        return get(new File(path), cls);
    }

    public <T> T get(File file, Class<T> cls) {
        if(!file.exists() || file.isDirectory()) {
            noFileError = true;
        } else {
            noFileError = false;
        }

        T t;

        try (JsonParser jsonParser = jsonFactory.createParser(file)) {
            jsonParser.setCodec(new ObjectMapper().enableDefaultTyping());
            t = jsonParser.readValueAs(cls);
            parseError = false;
        } catch (UnrecognizedPropertyException e) {
            t = null;
            parseError = false;
        } catch (IOException e) {
            t = null;
            parseError = true;
        }

        return t;
    }

    public JsonFactory getJsonFactory() {
        return this.jsonFactory;
    }

}
