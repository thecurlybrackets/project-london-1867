package de.thecurlybrackets.londonmaker.json;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonWriter {

    private JsonFactory jsonFactory;
    private JsonGenerator jsonGenerator;

    public JsonWriter() {
        this.jsonFactory = new JsonFactory();
    }

    public JsonWriter(JsonFactory jsonFactory) {
        this.jsonFactory = jsonFactory;
    }

    public <T> void newJsonFile(String path, T t) throws IOException {
        newJsonFile(new File(path), t);
    }

    public <T> void newJsonFile(File file, T t) throws IOException {
        jsonGenerator = jsonFactory.createGenerator(file, JsonEncoding.UTF8);
        jsonGenerator.useDefaultPrettyPrinter();
        jsonGenerator.setCodec(new ObjectMapper().enableDefaultTyping());

        jsonGenerator.writeObject(t);

        jsonGenerator.close();
    }

    public JsonFactory getJsonFactory() {
        return this.jsonFactory;
    }

}
