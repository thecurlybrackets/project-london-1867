package de.thecurlybrackets.londonmaker.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.thecurlybrackets.londonmaker.tools.ActionOne;

import java.lang.reflect.Array;

public class DynamicJsonArray<T> {

    private final int OVERFLOW_VALUE = 4;

    @JsonProperty
    private T[] items;

    @JsonProperty /*check if differences to when not there*/
    private int size;

    @JsonIgnore
    private Class<T> cls;

    public DynamicJsonArray() {} //is needed for Jackson-API and shall not be used

    public DynamicJsonArray(Class<T> cls) {
        this.cls = cls;

        this.items = newArray();
        this.size = 0;
    }

    public DynamicJsonArray(T[] items, Class<T> cls) {
        this.cls = cls;

        this.items = items;
        this.size = items.length;
    }

    public DynamicJsonArray addAll(T... items) {
        for(T item : items) {
           add(item);
        }
        return this;
    }

    public DynamicJsonArray add(T item) {
        if (items.length == size) {
            resize(OVERFLOW_VALUE);
        }
        items[size] = item;
        size++;
        return this;
    }

    public T remove(int index) {
        if(items.length == size + OVERFLOW_VALUE) {
            fit();
        }
        size--;
        return fillGap(index);
    }

    public int getSize() {
        return size;
    }

    private void resize(int value) {
        T[] tmp = newArray(items.length + value);
        System.arraycopy(items, 0, tmp, 0, items.length);
        items = tmp;
    }

    private void fit() {
        T[] tmp = newArray(size);
        System.arraycopy(items, 0, tmp, 0, size);
        items = tmp;
    }

    private T fillGap(int index) {
        T tmp = items[index];
        for (;index < size; index++) {
            items[index] = items[index+1];
        }
        items[index] = null;
        return tmp;
    }

    public void foreach(ActionOne<T> item) {
        for (int i = 0; i < size; i++) {
            item.execute(items[i]);
        }
    }

    private T[] newArray() {
        return newArray(0);
    }

    private T[] newArray(int size) {
        @SuppressWarnings("unchecked")
        T[] t = (T[])Array.newInstance(cls, size);
        return t;
    }

}
