package de.thecurlybrackets.londonmaker.json.dataclasses;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class EditorFile {

    @JsonProperty
    public String segmentDirectoryPath;

    @JsonProperty
    public Map<String, String> segments;

    private EditorFile() {}

    public EditorFile(String segmentDirectoryPath) {
        this.segmentDirectoryPath = segmentDirectoryPath;
        this.segments = new HashMap<>();
    }

}
