package de.thecurlybrackets.londonmaker.json.dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.thecurlybrackets.londonmaker.json.DynamicJsonArray;
import de.thecurlybrackets.londonmaker.resource_bundle.settings.SettingsLoader;

import java.text.MessageFormat;

public class Segment { //A segment is one piece of the map

    @JsonProperty
    private int positionX, positionY;

    @JsonProperty
    private int width, height; //might not be used

    @JsonProperty
    private DynamicJsonArray<Room> rooms;

    private Segment() {} /* needed for Jackson API */

    public Segment(int positionX, int positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    @JsonIgnore
    public String getName() {
        return SettingsLoader.get("segment_file_name", positionX, positionY);
    }

    @JsonIgnore
    public Position getPosition() {
        return new Position(positionX, positionY);
    }

    @JsonIgnore
    public Segment combine(Segment segment) {

        //combine both segments

        return this;
    }

}
