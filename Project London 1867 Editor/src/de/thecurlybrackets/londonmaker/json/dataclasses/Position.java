package de.thecurlybrackets.londonmaker.json.dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.InputMismatchException;

public class Position {

    @JsonProperty
    public int x, y;

    private Position() {}

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @JsonIgnore
    public static String pattern = "\\d+,\\d+";

    @JsonIgnore
    public static Position parse(String string) {
        String[] divided = null;

        if(!string.matches(pattern)) { //throw Exception????
            //throw new InputMismatchException();
        } else {
            divided = string.split(",");
        }

        return new Position(Integer.parseInt(divided[0]), Integer.parseInt(divided[1]));
    }

    @JsonIgnore
    @Override
    public String toString() {
        return this.x + "," + this.y;
    }

}
