package de.thecurlybrackets.londonmaker.json.dataclasses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Room {

    @JsonProperty
    private Position location;

    @JsonProperty
    private String pathOfPlace;

    {
        //load place from placepath
    }

    public Room() {

    }

    public Room(Position location, String pathOfPlace) {
        this.location = location;
        this.pathOfPlace = pathOfPlace;
    }

    public Position getLocation() {
        return location;
    }

    public String getPathOfPlace() {
        return pathOfPlace;
    }

}
