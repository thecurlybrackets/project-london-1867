package de.thecurlybrackets.londonmaker.json;

import com.fasterxml.jackson.core.JsonFactory;
import de.thecurlybrackets.londonmaker.json.dataclasses.EditorFile;
import de.thecurlybrackets.londonmaker.json.dataclasses.Position;
import de.thecurlybrackets.londonmaker.json.dataclasses.Segment;
import de.thecurlybrackets.londonmaker.tools.ActionTwo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class JsonOrganizer {

    public static String EDITOR_FILE_EXTENSION;
    public static String SEGMENT_FILE_EXTENSION;

    private File editorFile;
    private File segmentDirectory;

    private JsonFactory jsonFactory;
    private JsonWriter jsonWriter;
    private JsonReader jsonReader;

    private EditorFile editorFileObject;

    private ArrayList<Boolean> changedSegments; //maybe as Integer, maybe not needed
    private ArrayList<Boolean> deletedSegments; //maybe as Integer, maybe not needed

    //private Set<Segment> addedSegments;

    /*static {
        EDITOR_FILE_EXTENSION = SettingsLoader.get("editor_file_extension");
        SEGMENT_FILE_EXTENSION = SettingsLoader.get("segment_file_extension");
    }

    public JsonOrganizer() throws IOException {

        jsonFactory = new JsonFactory();
        jsonWriter = new JsonWriter(jsonFactory);
        jsonReader = new JsonReader(jsonFactory);

    }

    public void initSaveAs(String editorFilePath, String segmentDirectoryPath) throws IOException {

        init(editorFilePath, segmentDirectoryPath, newSegmentDirectoryPath -> {
            editorFileObject = new EditorFile(newSegmentDirectoryPath);
        }, newSegmentDirectoryPath -> {
            editorFileObject.segmentDirectoryPath = newSegmentDirectoryPath;
        }, (newEditorFilePath, newSegmentDirectoryPath) -> {
            editorFile = new File(newEditorFilePath);
            editorFileObject = new EditorFile(newSegmentDirectoryPath);
        });

    }

    public boolean initOpen(String editorFilePath) throws IOException {
        return init(editorFilePath, "", newSegmentDirectoryPath -> {
            new ErrorAlert(LanguageLoader.get("error_alert_title"), LanguageLoader.get("error_alert_message"));
        }, s -> {}, (s, t) -> {});
    }

    private boolean init(String editorFilePath, String segmentDirectoryPath, ActionOne<String> onCorrupted, ActionOne<String> onSuccess,  ActionTwo<String, String> onCorruptedFilePath) throws IOException {

        if(checkFile(editorFilePath, EDITOR_FILE_EXTENSION)) {

            editorFile = new File(editorFilePath);
            editorFileObject = jsonReader.get(editorFile, EditorFile.class);

            if(editorFileObject == null) {
                onCorrupted.execute(segmentDirectoryPath);
            } else {
                onSuccess.execute(segmentDirectoryPath);
                return true;
            }

        } else {

            onCorruptedFilePath.execute(editorFilePath, segmentDirectoryPath);

        }

        return false;

    }

    private boolean checkFile(String path, String extension) {

        if(path == null) {
            return false;
        }

        File file = new File(path);

        if(!file.exists()) {
            return false;
        }

        return file.getName().matches("(.*)" + extension);
    }*/

    public JsonOrganizer addSegment(Segment segment) throws IOException {

        if(editorFileObject.segments.put(segment.getPosition().toString(), segment.getName()) != null) {
            //key already exists
        } else {
            //key doesent exist
        }

        //create Segment File, but be aware of two segments at the same position!!!

        return this;
    }

    public JsonOrganizer addSegments(Segment... segments) throws IOException {
        for(Segment segment : segments) {
            addSegment(segment);
        }
        return this;
    }

    public JsonOrganizer removeSegment(Position position, String name) {
        editorFileObject.segments.remove(position.toString(), name);

        //remove Segment File

        return this;
    }

    public JsonOrganizer removeSegment(Position position) {
        editorFileObject.segments.remove(position.toString());

        //remove Segment File

        return this;
    }

    public void foreachSegment(ActionTwo<Position, String> item) { //method may be renamed
        editorFileObject.segments.forEach((position, name) -> {
            item.execute(Position.parse(position), name);
        });
    }

    public void updateFiles() throws IOException {
        jsonWriter.newJsonFile(editorFile, editorFileObject); //rewrite editor file
    }

    public String createFilePathOf(File path, String name, boolean isSegment) {
        return createFilePathOf(path.toString(), name, isSegment);
    }

    public String createFilePathOf(String path, String name, boolean isSegment) {
        if(isSegment) return segmentDirectory + "\\" + name + SEGMENT_FILE_EXTENSION;
        else return segmentDirectory + "\\" + name + EDITOR_FILE_EXTENSION;
    }

}
