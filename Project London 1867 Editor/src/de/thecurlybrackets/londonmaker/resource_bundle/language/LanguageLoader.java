package de.thecurlybrackets.londonmaker.resource_bundle.language;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class LanguageLoader {

    private static String resourceBundleLocation;
    private static ResourceBundle resourceBundle;

    static {

        resourceBundleLocation = "de.thecurlybrackets.londonmaker.resource_bundle.language.lang";
        resourceBundle = ResourceBundle.getBundle(resourceBundleLocation);

    }

    public static String get(String key) {
        return resourceBundle.getString(key);
    }

    public static <T extends Number> String get(String key, T... t) {
        if(t == null) throw new NullPointerException();
        return MessageFormat.format(get(key), (Object)t);
    }

    public static String get(String key, String... s) {
        if(s == null) throw new NullPointerException();
        return MessageFormat.format(get(key), (Object)s);
    }

    public static void setLanguage(Language lang) {
        Locale.setDefault(lang.asLocale());
        resourceBundle = ResourceBundle.getBundle(resourceBundleLocation);
    }

    public static Language getCurrentLanguage() {
        return Language.localeToLanguage(Locale.getDefault());
    }

}
