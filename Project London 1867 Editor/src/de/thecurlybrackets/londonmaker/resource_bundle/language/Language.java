package de.thecurlybrackets.londonmaker.resource_bundle.language;

import java.util.Locale;

public enum Language {
    US_ENGLISH("", "", "American English"),
    DE_GERMAN("de", "DE", "Deutsch");

    public final String LANGUAGE_ABBREVATION;
    public final String COUNTRY_ABBREVATION;
    public final String LANGUAGE;

    Language(String languageAbbreviation, String countryAbbreviation, String language) {
        this.LANGUAGE_ABBREVATION = languageAbbreviation;
        this.COUNTRY_ABBREVATION = countryAbbreviation;
        this.LANGUAGE = language;
    }

    public Locale asLocale() {
        return new Locale(LANGUAGE_ABBREVATION, COUNTRY_ABBREVATION);
    }

    public static Language localeToLanguage(Locale local) {

        switch (local.getCountry()) {
            case "":
                return US_ENGLISH;
            case "DE":
                return DE_GERMAN;
            default:
                return null;
        }

    }

    @Override
    public String toString() {
        return LANGUAGE;
    }
}
