package de.thecurlybrackets.londonmaker.file;

import java.io.File;

public class Directory extends File {

    public Directory(String pathname) {
        super(pathname);
    }

    @Override
    public boolean exists() {
        return super.exists() && isDirectory();
    }

}
