package de.thecurlybrackets.londonmaker.file;

public class Extension {

    private final String extension;

    public Extension(String extension) {
        if(extension == null || extension.isBlank()) {
            this.extension = "";
        } else if(extension.charAt(0) == '.') {
            this.extension = extension.substring(1);
        } else {
            this.extension = extension;
        }
    }

    public String getExtension() {
        return extension;
    }

    @Override
    public String toString() {
        return (extension.isBlank()) ? "" : '.' + extension;
    }

    @Override
    public boolean equals(Object obj) {

        if(obj instanceof Extension) {
            return extension.equals(((Extension) obj).getExtension());
        } else if(obj instanceof String) {
            return toString().equals(obj) || extension.equals(obj);
        }

        return false;
    }

    public static Extension extractFrom(File file) {
        String name = file.getName();

        if(!name.contains(".")) {
            return new Extension("");
        }

        return new Extension(name.substring(name.lastIndexOf(".")));

    }
}
