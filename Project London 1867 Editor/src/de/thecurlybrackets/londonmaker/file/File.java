package de.thecurlybrackets.londonmaker.file;

import de.thecurlybrackets.londonmaker.json.JsonReader;

import java.io.IOException;

public class File extends java.io.File {

    public File(String pathname) {
        super(pathname);
    }

    @Override
    public boolean exists() {
        return super.exists() && isFile();
    }

    public boolean hasExtension(String ext) {
        return Extension.extractFrom(this).equals(ext);
    }

    public boolean hasExtension(Extension ext) {
        return Extension.extractFrom(this).equals(ext);
    }

    public <T> boolean matchesTemplate(Class<T> templateClass) throws IOException {
        return new JsonReader().get(this, templateClass) != null;
    }

}
