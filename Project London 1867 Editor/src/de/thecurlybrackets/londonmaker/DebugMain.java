package de.thecurlybrackets.londonmaker;

import de.thecurlybrackets.londonmaker.file.Extension;
import de.thecurlybrackets.londonmaker.file.File;
import de.thecurlybrackets.londonmaker.json.dataclasses.EditorFile;

import java.io.IOException;

public class DebugMain {

    public static void main(String[] args) throws IOException {

        Extension extension = new Extension("londonedit");

        File file = new File("D:\\Daten\\Programming\\Java\\London 1867 git\\test\\test" + extension);

        //JsonWriter jsonWriter = new JsonWriter();
        //jsonWriter.newJsonFile(file, new EditorFile("test"));

        System.out.println(file.matchesTemplate(EditorFile.class));

    }

}
