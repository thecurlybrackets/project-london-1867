package de.thecurlybrackets.londonmaker.tools;

public interface ActionOne<T> {

    void execute(T t);

}

