package de.thecurlybrackets.londonmaker.tools;

public interface ActionTwo<T, U> {

    void execute(T t, U u);

}
