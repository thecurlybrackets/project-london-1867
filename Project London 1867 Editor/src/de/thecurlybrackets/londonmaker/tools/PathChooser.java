package de.thecurlybrackets.londonmaker.tools;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PathChooser {

    public static File openDirectory(String windowTitle, File initialDirectory) {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle(windowTitle);
        if(initialDirectory != null && initialDirectory.isDirectory()) {
            dirChooser.setInitialDirectory(initialDirectory);
        }
        return dirChooser.showDialog(null);
    }

    public static String openDirectoryReturnString(String windowTitle, File initialDirectory) {
        File file = openDirectory(windowTitle, initialDirectory);

        if (file == null) {
            return "";
        }else {
            return file.getPath();
        }
    }

    private static FileChooser chooseFile(String windowTitle, File initialDirectory, FileChooser.ExtensionFilter... extensionFilters) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(windowTitle);
        if(initialDirectory != null && initialDirectory.isDirectory()) {
            fileChooser.setInitialDirectory(initialDirectory);
        }
        fileChooser.getExtensionFilters().addAll(extensionFilters);
        return fileChooser;
    }

    public static File openFile(String windowTitle, File initialDirectory, FileChooser.ExtensionFilter... extensionFilters) {
        return chooseFile(windowTitle, initialDirectory, extensionFilters).showOpenDialog(null);
    }

    public static String openFileReturnString(String windowTitle, File initialDirectory, FileChooser.ExtensionFilter... extensionFilters) {
        File file = openFile(windowTitle, initialDirectory, extensionFilters);

        if (file == null) {
            return "";
        }else {
            return file.getPath();
        }
    }

    public static List<File> openFiles(String windowTitle, File initialDirectory, FileChooser.ExtensionFilter... extensionFilters) {
        return chooseFile(windowTitle, initialDirectory, extensionFilters).showOpenMultipleDialog(null);
    }

    public static List<String> openFilesReturnString(String windowTitle, File initialDirectory, FileChooser.ExtensionFilter... extensionFilters) {
        List<File> files = openFiles(windowTitle, initialDirectory, extensionFilters);

        if (files == null) {
            return null;
        }else {

            List<String> paths = new ArrayList<>();
            for (File file : files) {
                paths.add(file.getPath());
            }
            return paths;
        }
    }

    public static File saveFile(String windowTitle, File initialDirectory, FileChooser.ExtensionFilter... extensionFilters) {
        return chooseFile(windowTitle, initialDirectory, extensionFilters).showSaveDialog(null);
    }

    public static String saveFileReturnString(String windowTitle, File initialDirectory, FileChooser.ExtensionFilter... extensionFilters) {
        File file = saveFile(windowTitle, initialDirectory, extensionFilters);

        if (file == null) {
            return "";
        }else {
            return file.getPath();
        }
    }

}
