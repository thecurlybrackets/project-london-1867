package de.thecurlybrackets.londonmaker;

import de.thecurlybrackets.londonmaker.graphics.Window;

public class Main {

	public static void main(String[] args) {

		Window.run(args);

	}

}
