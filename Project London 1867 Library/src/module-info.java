/**
 * Denotes the interface between anything requiring
 * the components of London 1867
 */
module londonlibrary {
	exports de.thecurlybrackets.londonlib.game;
	exports de.thecurlybrackets.londonlib.game.exceptions;
	exports de.thecurlybrackets.londonlib.game.gui;
	exports de.thecurlybrackets.londonlib.game.worldobjects;
	exports de.thecurlybrackets.londonlib.game.worldobjects.attributes;
	exports de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities;
	exports de.thecurlybrackets.londonlib.game.worldobjects.attributes.props;
	exports de.thecurlybrackets.londonlib.game.worldobjects.entities;
	exports de.thecurlybrackets.londonlib.game.worldobjects.entities.components;
	exports de.thecurlybrackets.londonlib.game.worldobjects.places;
	exports de.thecurlybrackets.londonlib.game.worldobjects.props;
	exports de.thecurlybrackets.londonlib.game.worldobjects.props.items;
	exports de.thecurlybrackets.londonlib.parser;
	exports de.thecurlybrackets.londonlib.parser.components;
	exports de.thecurlybrackets.londonlib.parser.exceptions;
	exports de.thecurlybrackets.londonlib.parser.translation;
	requires transitive java.xml;
	requires java.desktop;
}