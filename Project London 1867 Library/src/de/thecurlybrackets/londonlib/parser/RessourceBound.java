package de.thecurlybrackets.londonlib.parser;

import java.io.File;

public interface RessourceBound {
	
	/**
	 * 
	 * @return the directory to the ressources
	 */
	public File getRessourcesDirectory();
	
}
