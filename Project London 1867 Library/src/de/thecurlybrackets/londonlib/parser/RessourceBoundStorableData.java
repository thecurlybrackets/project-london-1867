package de.thecurlybrackets.londonlib.parser;

import java.io.File;
import java.io.IOException;


/**
 * In case storable data depends on resources in
 * a directory
 * 
 * @author Tim Roethig
 *
 */
public abstract class RessourceBoundStorableData<TDataFormat> extends StorableData<TDataFormat> implements RessourceBound {
	
	/**
	 * The directory of the resources
	 */
	protected File ressourcesDir;
	
	public RessourceBoundStorableData(File loadFile, File ressourcesDir, TDataFormat formattedData) throws IllegalArgumentException, IOException {
		super(loadFile, formattedData);
		ressourcesDir.mkdirs();
		if(!ressourcesDir.isDirectory()) {
			throw new IllegalArgumentException("The given ressources-directory is no directory");
		}
	}
	
	@Override
	public File getRessourcesDirectory() {
		return this.ressourcesDir;
	}

}
