/**
 * 
 * Most types abstracting parsing-processes
 * and handling general I/O on drives
 * 
 * @author Tim Roethig
 * @see de.thecurlybrackets.londonlib.game.worldobjects.XMLContent
 */
package de.thecurlybrackets.londonlib.parser;

