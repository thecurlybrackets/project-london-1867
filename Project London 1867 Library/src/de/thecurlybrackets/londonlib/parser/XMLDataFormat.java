package de.thecurlybrackets.londonlib.parser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;

/**
 * A structure summarizing the basic
 * needs for any Object using XML-Documents
 * 
 * @author Tim Roethig
 *
 */
public class XMLDataFormat {
	
	public DocumentBuilder builder;
	public Transformer transformer;
	
	public static final DocumentBuilderFactory GLOBAL_XMLDOCBUILDER_FACTORY = DocumentBuilderFactory.newInstance();
	public static final TransformerFactory GLOBAL_XMLTRANSFORMER_FACTORY = TransformerFactory.newInstance();
	
	/**
	 * Creates a default set of objects for the usage of
	 * {@link org.w3c.dom.Document XMl-Documents}
	 * @throws ParserConfigurationException
	 */
	public XMLDataFormat() throws ParserConfigurationException {
		this.builder = GLOBAL_XMLDOCBUILDER_FACTORY.newDocumentBuilder();
		try {
			this.transformer = GLOBAL_XMLTRANSFORMER_FACTORY.newTransformer();
			this.transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			this.transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
