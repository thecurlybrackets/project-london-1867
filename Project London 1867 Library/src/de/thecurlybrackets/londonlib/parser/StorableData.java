package de.thecurlybrackets.londonlib.parser;

import java.io.File;
import java.io.IOException;

import de.thecurlybrackets.londonlib.parser.exceptions.SaveObjectToFileFailureException;

/**
 * This provides the basic capabilities
 * of an object which can be saved on a drive.
 * 
 * @author Tim Roethig
 *
 */
public abstract class StorableData<TDataFormat> implements Storable {
	/**
	 * The file in which all data is stored
	 */
	protected File refFile;
	protected TDataFormat formattedData;
	
	/**
	 * The (meta-)data of the file
	 * this data is stored in
	 * @return Information as a File
	 */
	public File getReferringFile() {
		return this.refFile;
	}
	
	/**
	 * Loads an existing file
	 * 
	 * @param loadFile The existing file
	 * @throws IOException Redirection
	 * @throws IllegalArgumentException Redirection 
	 */
	public StorableData(File loadFile, TDataFormat formattedData) throws IllegalArgumentException, IOException {
		this.formattedData = formattedData;
		this.refFile = loadFile;
		loadFile.getParentFile().mkdirs();
		if(!loadFile.exists()) {
			loadFile.createNewFile();
			this.loadContents(loadFile);
		}
	}
	
	public TDataFormat getFormattedData() {
		return this.formattedData;
	}
	
	@Override
	public abstract String getFileExtension();
	
	@Override
	public void saveContents() throws SaveObjectToFileFailureException {
		this.saveContents(this.refFile);
	}
	
	@Override
	public abstract void saveContents(File file) throws SaveObjectToFileFailureException;
	
	@Override
	public abstract void loadContents(File file) throws IllegalArgumentException, IOException;
	
	@Override
	public void reloadContents() throws IllegalArgumentException, IOException {
		this.loadContents(this.refFile);
	}
}
