package de.thecurlybrackets.londonlib.parser;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * A GameConfig contains all game-configurations,
 * thus they can be saved and altered.
 * 
 * It is based on a {@link Properties} and uses
 * the file-extension .config 
 * 
 * @author Tim Roethig
 *
 */
public abstract class Config extends StorableData<Properties> {
	
	public static final String CONFIG_EXTENSION = ".config";
	
	public Config(File loadFile) throws IllegalArgumentException, IOException {
		super(loadFile, new Properties());
		// TODO Auto-generated constructor stub
	} 
	

	@Override
	public String getFileExtension() {
		return CONFIG_EXTENSION;
	}

	@Override
	public void saveContents() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void saveContents(File file) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void loadContents(File file) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
