package de.thecurlybrackets.londonlib.parser.translation;

import java.io.File;
import java.io.IOException;

import de.thecurlybrackets.londonlib.parser.RessourceBoundStorableData;
import de.thecurlybrackets.londonlib.parser.XMLDataFormat;

/**
 * A Translatable is any class
 * containing translatable data
 * and an identifier for readable
 * access
 * 
 * @author Tim Roethig
 *
 */
public abstract class Translatable extends RessourceBoundStorableData<XMLDataFormat> implements TranslationInvoker {
	
	/**
	 * The structured translation
	 */
	protected LondonTongue translation;
	
	public Translatable(File loadFile, File ressourcesFile, XMLDataFormat formattedData) throws IllegalArgumentException, IOException {
		super(loadFile, ressourcesFile, formattedData);
	}
	
}
