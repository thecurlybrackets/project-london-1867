package de.thecurlybrackets.londonlib.parser.translation;

import java.util.Locale;

import de.thecurlybrackets.londonlib.parser.exceptions.TranslationFailureException;

/**
 * This interface allows classes
 * to invoke a translation-process
 * without being translatable itself
 * @author Tim Roethig
 *
 */
public interface TranslationInvoker {
	/**
	 * The default subdirectory of, for example,
	 * parts or mods
	 */
	public static final String DEFAULT_LANG_SUBDIR = "/lang/";
	
	/**
	 * Invokes the translation-process
	 * @param locale The Locale to translate to
	 * @throws TranslationFailureException In case the translation-process fails
	 */
	public abstract void translateTo(Locale locale) throws TranslationFailureException;
}
