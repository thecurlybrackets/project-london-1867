package de.thecurlybrackets.londonlib.parser.translation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import de.thecurlybrackets.londonlib.parser.StorableData;
import de.thecurlybrackets.londonlib.parser.exceptions.SaveObjectToFileFailureException;

/**
 * A LondonTongue defines and saves all
 * text for any Translatable.
 * 
 * Its default location on drive is
 * TRANSLATABLE_TYPE_DIR/lang/NAME/LOCALE.londontongue
 * 
 * @author Tim Roethig
 * @see Translatable
 *
 */
public class LondonTongue extends StorableData<Properties> {

	/**
	 * The current language-specifier (e.g.: de_DE)
	 */
	protected Locale currentLocale = LondonTongue.DEFAULT_LOCALE;
	
	/**
	 * The currently used encoding
	 */
	protected String enconding = LondonTongue.DEFAULT_ENCODING;
	
	/**
	 * The used default-encoding for the Properties-file for
	 * this language
	 */
	public static final String DEFAULT_ENCODING = "UTF-8";
	
	/**
	 * The extension of any LondonTongue-File
	 */
	public static final String TONGUE_EXTENSION = ".londontongue";
	
	/**
	 * The default locale (currently "de_DE" but may be changed to "en_UK")
	 */
	public static final Locale DEFAULT_LOCALE = Locale.GERMANY;
	
	/**
	 * Creates a LondonTongue using a specific Locale
	 * @param pathToRessources
	 * @param desiredLocale
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	public LondonTongue(String pathToRessources, Locale desiredLocale) throws IllegalArgumentException, IOException {
		super(new File(pathToRessources + TranslationInvoker.DEFAULT_LANG_SUBDIR + desiredLocale + TONGUE_EXTENSION), new Properties());
		this.loadContents(this.refFile);
	}
	
	/**
	 * Creates a LondonTongue using the default Locale
	 * @param pathToRessources
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @see LondonTongue#DEFAULT_LOCALE
	 */
	public LondonTongue(String pathToRessources) throws IllegalArgumentException, IOException {
		this(pathToRessources, DEFAULT_LOCALE);
	}
	
	@Override
	public String getFileExtension() {
		return TONGUE_EXTENSION;
	}
	
	@Override
	public void saveContents() throws SaveObjectToFileFailureException {
		this.saveContents(this.refFile);
	}
	
	@Override
	public void saveContents(File file) throws SaveObjectToFileFailureException {
		FileOutputStream ofs;
		try {
			ofs = new FileOutputStream(file);
			this.formattedData.store(ofs, this.currentLocale.toString());
		} catch (IOException e) {
			throw new SaveObjectToFileFailureException(this, file, e.getMessage());
		}
		return;
	}

	@Override
	public void loadContents(File file) throws IllegalArgumentException, IOException {
		this.refFile = file;
		FileInputStream ifs = new FileInputStream(file);
		this.formattedData.load(ifs);
		//the filename without the extension
		int localeEnd = file.getName().indexOf('.');
		if(localeEnd > 0) {
			this.currentLocale = new Locale(file.getName().substring(0, localeEnd));
		} else {
			throw new IllegalArgumentException("Invalid filename!");
		}
	}
	
	

}
