/**
 * Allows the translation from one Locale to another
 * @see java.util.Locale
 */
package de.thecurlybrackets.londonlib.parser.translation;