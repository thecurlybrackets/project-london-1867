package de.thecurlybrackets.londonlib.parser;

/**
 * OPTIONAL: A Logger is designed to enable
 * a class-specific logging-scheme
 * 
 * @author Tim Roethig
 * 
 */
public interface Logger {
	
	/**
	 * Allows a custom logging-behavior
	 * for specified objects
	 * 
	 * @param obj Object
	 * @param msg Message
	 */
	void log(Object obj, String msg);
	
}
