package de.thecurlybrackets.londonlib.parser.exceptions;

import java.io.File;
import java.util.Locale;

/**
 * Thrown when a translation-process fails
 * @author Tim Roethig
 *
 */
public class TranslationFailureException extends Exception {

	private static final long serialVersionUID = -6928821092828856793L;
	
	protected Locale locale;
	
	/**
	 * Creates this exception-type given the
	 * affected Locale and File
	 * @param locale The locale of the affected FIle
	 * @param which The affected File
	 */
	public TranslationFailureException(Locale locale, File which) {
		super("The LondonTongue \"" + which.getPath() + "\" could not be loaded!");
		this.locale = locale;
	}
	
	/**
	 * Meant for handling certain Locale-s
	 * (e.g.: check whether the locale is default or not)
	 * @return The Locale of the affected File
	 */
	public Locale getLocale() {
		return this.locale;
	}
	
}
