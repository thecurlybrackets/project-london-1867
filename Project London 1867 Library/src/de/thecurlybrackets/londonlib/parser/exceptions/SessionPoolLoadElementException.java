package de.thecurlybrackets.londonlib.parser.exceptions;

import java.io.IOException;

import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.parser.components.SessionPoolElement;

/**
 * Is thrown when
 * {@link de.thecurlybrackets.londonlib.parser.components.SessionPool#provideLondonMod(Label, de.thecurlybrackets.londonlib.game.LondonWorld)
 * SessionPool.provideLondonMod(Label,LondonWorld)} or
 * {@link de.thecurlybrackets.londonlib.parser.components.SessionPool#provideLondonPart(Label, de.thecurlybrackets.londonlib.parser.components.LondonMod)
 * SessionPool.provideLondonPart(Label,LondonWorld)} fails
 * fails
 * @author Tim Roethig
 *
 */
public class SessionPoolLoadElementException extends IOException {

	private static final long serialVersionUID = 8172367164727936213L;
	
	/**
	 * Creates a default message telling which element
	 * (of what sort of type) could not be loaded
	 * @param what What sort of {@link SessionPoolElement} could not be loaded
	 * @param which {@link Label} of the {@link SessionPoolElement}
	 */
	public SessionPoolLoadElementException(Class<? extends SessionPoolElement> what, Label which) {
		super("Could not load " + what.getSimpleName() + "\"" + which + "\"");
	}
	
}
