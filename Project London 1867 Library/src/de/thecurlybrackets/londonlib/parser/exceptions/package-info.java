/**
 * 
 * Conatins all custom exceptions occurring during
 * the parsing process
 * 
 * @author Tim Roethig
 * 
 */
package de.thecurlybrackets.londonlib.parser.exceptions;

