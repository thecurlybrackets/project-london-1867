package de.thecurlybrackets.londonlib.parser.exceptions;

import java.io.File;
import java.io.IOException;

/**
 * Is thrown when {@link de.thecurlybrackets.londonlib.parser.Storable#saveContents(File) Storable.saveContents(File)}
 * fails
 * @author Tim Roethig
 *
 */
public class SaveObjectToFileFailureException extends IOException {

	private static final long serialVersionUID = -5801960357563718132L;
	
	/**
	 * Creates a default message telling
	 * which object could not be saved to a certain file
	 * whilst providing further details
	 * @param obj the {@link Object} which could not be loaded
	 * @param file the {@link File} it was supposed to be saved to
	 * @param details further details
	 */
	public SaveObjectToFileFailureException(Object obj, File file, String details) {
		super("Could not save this " + obj.getClass().getSimpleName() + " in " + file.getAbsolutePath() + " : " + details);
	}

}
