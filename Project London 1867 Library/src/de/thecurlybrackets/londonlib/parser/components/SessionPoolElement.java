package de.thecurlybrackets.londonlib.parser.components;

/**
 * Any element being part a {@link SessionPool} wherefore
 * it loads the pool it is a part of
 * @author Tim Roethig
 *
 */
public interface SessionPoolElement extends SessionPoolLoader {

}
