package de.thecurlybrackets.londonlib.parser.components;

import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.Entity;
import de.thecurlybrackets.londonlib.game.worldobjects.props.Prop;

/**
 * Represents an object providing 
 * the components of a {@link LondonPart}
 * externally, so it targets a LondonPart first
 * and then returns the requested object of it
 * by class
 * 
 * @author Tim Roethig
 *
 */
public interface LondonPartComponentProvider {
	
	/**
	 * Looks for an {@link Entity} named {@code label}
	 * in a {@link LondonPart} labeled {@code identifier}
	 * and then returns it
	 * 
	 * @param identifier {@link Label} of the {@link LondonPart} to look in
	 * @param label {@link Label} of the {@link Entity}
	 * @return if not successfully found {@code null} otherwise the requested {@link Entity}
	 */
	public Entity getEntity(Label identifier, Label label);
	
	/**
	 * Looks for a {@link Prop} named {@code label}
	 * in a {@link LondonPart} labeled {@code identifier}
	 * and then returns it
	 * 
	 * @param identifier {@link Label} of the {@link LondonPart} to look in
	 * @param label {@link Label} of the {@link Prop}
	 * @return if not successfully found {@code null} otherwise the requested {@link Prop}
	 */
	public Prop getProp(Label identifier, Label label);
	
}
