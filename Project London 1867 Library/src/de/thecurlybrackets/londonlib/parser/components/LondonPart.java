package de.thecurlybrackets.londonlib.parser.components;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.VastlyParented;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.Entity;
import de.thecurlybrackets.londonlib.game.worldobjects.props.Prop;
import de.thecurlybrackets.londonlib.parser.XMLDataFormat;
import de.thecurlybrackets.londonlib.parser.exceptions.TranslationFailureException;

/**
 * Defines either independent or strongly coherent Entity-s and Prop-s,
 * so they can function as basic components for building a LondonMod
 * @author Tim Roethig
 * @see LondonMod
 *
 */
public class LondonPart extends ComponentCollection implements VastlyParented<LondonMod> {
	protected SessionPool sessionPool;
	protected HashMap<Label, LondonMod> parentMods 	= new HashMap<>();
	protected HashMap<Label, Prop> props 		= new HashMap<>();
	protected HashMap<Label, Entity> entities 	= new HashMap<>();
	
	/* filesystem */
	public static final String PARTS_DIR = "parts/";
	public static final String PART_EXTENSION = ".londonpart";
	
	/* node-names */
	public static final String NODENAME_ENTITIES = "Entities";
	public static final String NODENAME_PROPS = "Props";
	
	public LondonPart(Label partName, SessionPool sessionPool) throws IllegalArgumentException, IOException, ParserConfigurationException {
		super(partName,	new File(PARTS_DIR + partName + PART_EXTENSION),
						new File(PARTS_DIR + partName + "/"), new XMLDataFormat());
		this.sessionPool = sessionPool;
	}
	
	public LondonPart(String partName, SessionPool sessionPool) throws IllegalArgumentException, IOException, ParserConfigurationException {
		this(new Label(partName), sessionPool);
	}
	
	/**
	 * Provides a Prop by its Label
	 * @param label The Label of the requested Prop
	 * @return The requested Prop or {@code null} if it was not found
	 */
	public Prop getProp(Label label) {
		return this.props.get(label);
	}
	
	/**
	 * Provides an Entity by its Label
	 * @param label The Label of the requested Entity
	 * @return The requested Entity or {@code null} if it was not found
	 */
	public Entity getEntity(Label label) {
		return this.entities.get(label);
	}
	
	/**
	 * Adds a Prop to this LondonPart
	 * @param prop The Prop to be added
	 */
	public void addElement(Prop prop) {
		this.props.put(prop.getLabel(), prop);
		return;
	}
	
	/**
	 * Adds an Entity to this LondonPart
	 * @param entity The Entity to be added
	 */
	public void addElement(Entity entity) {
		this.entities.put(entity.getLabel(), entity);
		return;
	}
	
	/**
	 * Removes a Prop from this LondonPart by instance
	 * @param which The Prop to be removed
	 */
	public void removeElement(Prop which) {
		this.props.remove(which.getLabel());
		return;
	}
	
	/**
	 * Removes an Entity from this LondonPart by instance
	 * @param which The Entity to be removed
	 */
	public void removeElement(Entity which) {
		this.entities.remove(which.getLabel());
		return;
	}
	
	@Override
	public void translateTo(Locale locale) throws TranslationFailureException  {
		//TODO	
	}
	
	@Override
	public String getFileExtension() {
		return PART_EXTENSION;
	}
	
	@Override
	public void saveContents(File file) {
		try {
			Document doc = this.formattedData.builder.newDocument();
			Element root = doc.createElement(this.getClass().getSimpleName());
			//attribute "label"
			root.setAttribute(ATTRNAME_LABEL, this.label.getText());
			//begin node "Props"
			Element props = doc.createElement(NODENAME_PROPS);
			for(var i : this.props.values()) {
				props.appendChild(i.createXMLContents(doc));
			}
			//end node "Props"
			root.appendChild(props);
			
			//begin node "Entities"
			Element entities = doc.createElement(NODENAME_ENTITIES);
			for(var i : this.entities.values()) {
				entities.appendChild(i.createXMLContents(doc));
			}
			//end node "Entities"
			root.appendChild(entities);
			
			doc.appendChild(root);
			
			DOMSource domSource = new DOMSource(doc);
            StreamResult streamResult = new StreamResult(file);
 
            this.formattedData.transformer.transform(domSource, streamResult);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void loadContents(File file) throws IllegalArgumentException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addParent(LondonMod parent) {
		this.parentMods.put(parent.getLabel(), parent);
		return;
	}

	@Override
	public void removeParent(LondonMod parent) {
		this.parentMods.remove(parent.getLabel());
		return;
	}

	@Override
	public int getParentCount() {
		return this.parentMods.size();
	}

	@Override
	public SessionPool getSessionPool() {
		return this.sessionPool;
	}

	@Override
	public Object getChildKey() {
		return this.getLabel();
	}

	@Override
	public final HashMap<Label, LondonMod> getParents() {
		return this.parentMods;
	}
	
}
