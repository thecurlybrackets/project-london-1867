package de.thecurlybrackets.londonlib.parser.components;

/**
 * Loads a {@link SessionPool} in order to
 * access its parts
 * @author Tim Roethig
 *
 */
public interface SessionPoolLoader {
	
	/**
	 * 
	 * @return the loaded {@link SessionPool}
	 */
	public SessionPool getSessionPool();
	
}
