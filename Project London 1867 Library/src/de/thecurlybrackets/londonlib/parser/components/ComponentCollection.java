package de.thecurlybrackets.londonlib.parser.components;

import java.io.File;
import java.io.IOException;

import de.thecurlybrackets.londonlib.game.worldobjects.Child;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.Labeled;
import de.thecurlybrackets.londonlib.parser.XMLDataFormat;
import de.thecurlybrackets.londonlib.parser.translation.Translatable;

/**
 * Represents a {@link Labeled labeled} collection of various
 * {@link Component Components} being translatable as well as
 * part of a {@link SessionPool}
 * 
 * @author Tim Roethig
 *
 */
public abstract class ComponentCollection extends Translatable implements Labeled, SessionPoolElement, Child {
	
	/**
	 * The Label (can be seen as a name being constant in a certain context)
	 */
	protected Label label;
	
	public ComponentCollection(Label label, File loadFile, File ressourcesFiles, XMLDataFormat formattedData)
			throws IllegalArgumentException, IOException {
		super(loadFile, ressourcesFiles, formattedData);
		this.label = label;
	}
	
	@Override
	public Label getLabel() {
		return this.label;
	}
	
}
