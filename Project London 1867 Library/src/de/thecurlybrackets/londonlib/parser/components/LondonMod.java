package de.thecurlybrackets.londonlib.parser.components;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.LondonWorld;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.SingleParented;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.components.Quest;
import de.thecurlybrackets.londonlib.game.worldobjects.places.Place;
import de.thecurlybrackets.londonlib.parser.XMLDataFormat;

/**
 * A LondonMod loads LondonPart-s
 * and uses their contents for
 * building further components
 * 
 * @author Tim Roethig
 * @see LondonPart
 * @see SessionPool
 */
public class LondonMod extends ComponentCollection implements ComponentCollectionLoader<LondonPart>, 
													SingleParented<LondonWorld> {
	protected SessionPool sessionPool;
	protected HashMap<Label, LondonPart> childrenParts = new HashMap<>();
	protected LondonWorld parentWorld;
	protected HashMap<Label, Quest> quests = new HashMap<>();
	protected HashMap<Label, Place> places = new HashMap<>();
	
	/* delimiters */
	public static final char LIST_ATTRIBUTE_DELIMITER = '|';
	
	/* filesystem */
	public static final String MOD_EXTENSION = ".londonmod";
	public static final String MODS_DIR = "mods/";
	
	/* attribute-names */
	public static final String ATTRNAME_PARTS = "parts";
	
	public LondonMod(Label modName, SessionPool sessionPool, LondonWorld parentWorld) throws IllegalArgumentException, IOException, ParserConfigurationException {
		super(modName, 	new File(MODS_DIR + modName + MOD_EXTENSION),
						new File(MODS_DIR + modName + "/"), new XMLDataFormat());
		this.sessionPool = sessionPool;
		this.setParentLondonWorld(parentWorld);
	}
	
	public LondonMod(String modName, SessionPool sessionPool, LondonWorld parentWorld) throws IllegalArgumentException, IOException, ParserConfigurationException {
		this(new Label(modName), sessionPool, parentWorld);
	}
	
	/**
	 * Provides a Place by its Label
	 * @param label The Label of the requested Place
	 * @return The requested Place or {@code null} if it was not found
	 */
	public Place getPlace(Label label) {
		return this.places.get(label);
	}
	
	/**
	 * Provides a Quest by its Label
	 * @param label The Label of the requested Quest
	 * @return The requested Quest or {@code null} if it was not found
	 */
	public Quest getQuest(Label label) {
		return this.quests.get(label);
	}
	
	public void addElement(Place place) {
		
		this.places.put(place.getLabel(), place);
	}
	
	public void addElement(Quest quest) {
		//TODO
	}
	
	/**
	 * Sets the parent LondonWorld of this LondonMod
	 * @param parent The parent LondonWorld
	 */
	public void setParentLondonWorld(LondonWorld parent) {
		this.parentWorld = parent;
	}
	
	/**
	 * Checks whether it is used by a LondonWorld
	 * @return {@code true} if the parent LondonWorld is {@code null} otherwise {@code false}
	 */
	public boolean isLoadedByWorld() {
		return this.parentWorld != null;
	}
	
	@Override
	public String getFileExtension() {
		return MOD_EXTENSION;
	}

	@Override
	public void saveContents(File file) {
		try {
			Document doc = this.formattedData.builder.newDocument();
			Element root = doc.createElement(this.getClass().getSimpleName());
			//id="???"
			root.setAttribute(ATTRNAME_LABEL, this.label.getText());
			//separate codeblock for creating the mod-list
			{
				//in order to avoid a potential reallocation
				int partsLength = 0;
				for(var i : this.childrenParts.entrySet()) {
					partsLength += i.getValue().getLabel().getText().length() + 1; //ignore the additional 1 at the end of this loop
				}
				StringBuilder parts = new StringBuilder(partsLength);
				partsLength = 1; //reuse for counting
				for(var i : this.childrenParts.entrySet()) {
					parts.append(i.getValue().getLabel());
					if(partsLength < this.childrenParts.size()) {
						parts.append(LondonMod.LIST_ATTRIBUTE_DELIMITER);
					}
					partsLength++;
				}
				
				//parts="???|???|..."
				root.setAttribute(ATTRNAME_PARTS, parts.toString());
				doc.appendChild(root);
			}
			DOMSource domSource = new DOMSource(doc);
	        StreamResult streamResult = new StreamResult(file);
	        this.formattedData.transformer.transform(domSource, streamResult);
        
			
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void loadContents(File file) {
		// TODO Auto-generated method stub

	}

	@Override
	public void translateTo(Locale locale) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attachComponentCollection(LondonPart part) {
		part.addParent(this);
		this.childrenParts.put(part.getLabel(), part);
	}

	@Override
	public void detachComponentCollection(LondonPart part) {
		part.removeParent(this);
		this.childrenParts.remove(part.getLabel());
	}

	@Override
	public SessionPool getSessionPool() {
		return this.sessionPool;
	}

	@Override
	public Object getParentKey() {
		return this.getLabel();
	}
	
	/**
	 * Synonym
	 * @see #attachComponentCollection(LondonPart)
	 */
	@Override
	public void addChild(LondonPart child) {
		this.attachComponentCollection(child);
		return;
	}
	
	/**
	 * Synonym
	 * @see #detachComponentCollection(LondonPart)
	 */
	@Override
	public void removeChild(LondonPart child) {
		this.detachComponentCollection(child);
		return;
	}

	@Override
	public HashMap<Label, LondonPart> getChildren() {
		return this.childrenParts;
	}

	@Override
	public Label getChildKey() {
		return this.getLabel();
	}

	@Override
	public void setParent(LondonWorld parent) {
		this.parentWorld = parent;
	}

	@Override
	public LondonWorld getParent() {
		return this.parentWorld;
	}
}
