package de.thecurlybrackets.londonlib.parser.components;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.parser.Statistics;

/**
 * Tailored {@link Statistics} of a
 * {@link SessionPool}
 * @author Tim Roethig
 *
 */
public class SessionPoolStatistics implements Statistics {
	
	protected int partCount;
	protected int modCount;
	protected ArrayList<Entry<Label, ArrayList<Label>>> labeledCoherentLoads;
	
	/**
	 * Extracts statistical data from the raw parts and mods of a SessionPool
	 * @param parts {@link LondonPart LondonParts} as a {@link Set} of {@link Entry Entries}
	 * @param mods {@link LondonMod LondonMods} as a {@link Set} of {@link Entry Entries}
	 */
	public SessionPoolStatistics(Set<Entry<Label, LondonPart>> parts, Set<Entry<Label, LondonMod>> mods) {
		this.partCount = parts.size();
		this.modCount = mods.size();
		this.labeledCoherentLoads = new ArrayList<Entry<Label, ArrayList<Label>>>(this.modCount);
		for(var i : mods) {
			ArrayList<Label> itsParts = new ArrayList<>(i.getValue().childrenParts.size());
			for(var j : i.getValue().childrenParts.entrySet()) {
				itsParts.add(j.getKey());
			}
			this.labeledCoherentLoads.add(new AbstractMap.SimpleEntry<Label, ArrayList<Label>>(i.getKey(), itsParts));
		}
	}
	
	@Override
	public void printStats() {
		this.printStats(System.out);
	}
	
	@Override
	public void printStats(PrintStream stream) {
		//selects either singular or plural of 'mod'
		if(this.modCount != 1) {
			stream.print(this.modCount + " mods@");
		} else {
			stream.print(this.modCount + " mod@");
		}
		//selects either singular or plural of 'part'
		if(this.partCount != 1) {
			stream.print(this.partCount + " parts");
		} else {
			stream.print(this.partCount + " part");
		}
		//conjugates 'be'
		if(this.modCount != 1) {
			stream.println(" were loaded");
		} else {
			stream.println(" was loaded");
		}
		
		//printing the dependency of mods and their parts
		stream.println("Dependency Structure:");
		
		if(this.modCount == 0) {
			//in case there were no mods loaded
			stream.println("\tNo mods were loaded");
		}
		//listing all mods and their parts
		for(var i : this.labeledCoherentLoads) {
			//print mod's name
			stream.println("\t- " + i.getKey());
			//in case this mod has no parts loaded
			if(i.getValue().isEmpty()) {
				stream.println("\t\tThis mod loads no parts");
			}
			//list this mod's parts
			for(var j : i.getValue()) {
				stream.println("\t\t+ " + j);
			}
		}
	}
	
	@Override
	public String toString() {
		ByteArrayOutputStream ofstream = new ByteArrayOutputStream();
		this.printStats(new PrintStream(ofstream));
		return ofstream.toString();
	}
	
}
