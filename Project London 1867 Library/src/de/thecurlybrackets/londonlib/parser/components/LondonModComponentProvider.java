package de.thecurlybrackets.londonlib.parser.components;

import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.components.Quest;
import de.thecurlybrackets.londonlib.game.worldobjects.places.Place;

/**
 * Represents an object providing 
 * the components of a {@link LondonMod}
 * externally, so it targets a LondonMod first
 * and then returns the requested object of it
 * by class
 * 
 * @author Tim Roethig
 *
 */
public interface LondonModComponentProvider {
	
	/**
	 * Looks for an {@link Place} named {@code label}
	 * in a {@link LondonMod} labeled {@code identifier}
	 * and then returns it
	 * 
	 * @param identifier {@link Label} of the {@link LondonMod} to look in
	 * @param label {@link Label} of the {@link Place}
	 * @return if not successfully found {@code null} otherwise the requested {@link Place}
	 */
	public Place getPlace(Label identifier, Label label);
	
	/**
	 * Looks for an {@link Quest} named {@code label}
	 * in a {@link LondonMod} labeled {@code identifier}
	 * and then returns it
	 * 
	 * @param identifier {@link Label} of the {@link LondonMod} to look in
	 * @param label {@link Label} of the {@link Quest}
	 * @return if not successfully found {@code null} otherwise the requested {@link Quest}
	 */
	public Quest getQuest(Label identifier, Label label);
	
}
