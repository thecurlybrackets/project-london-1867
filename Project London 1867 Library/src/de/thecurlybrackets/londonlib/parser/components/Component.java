package de.thecurlybrackets.londonlib.parser.components;

import de.thecurlybrackets.londonlib.game.worldobjects.Label;

/**
 * Any object accumulated by a {@link ComponentCollection}
 * @author Tim Roethig
 *
 * @param <TComponentCollection> collection-type of which it is a component
 */
public interface Component<TComponentCollection extends ComponentCollection> {
	
	/**
	 * {@link #rename(Label) Renames} this component and
	 * {@link #moveTo(ComponentCollection) moves} it to the given
	 * {@code destination}
	 * @param label new {@link Label} of this component
	 * @param destination new component-collection of this component
	 */
	public default void bindTo(Label label, TComponentCollection destination) {
		this.rename(label);
		this.moveTo(destination);
		return;
	}
	
	/**
	 * Detaches this component from its current parent collection
	 * and attaches it to {@code collection}
	 * @param destination new component-collection of this component
	 */
	public void moveTo(TComponentCollection destination);
	
	/**
	 * Resets its {@link de.thecurlybrackets.londonlib.game.worldobjects.ID ID} using {@code label} as a new {@link Label}
	 * @param label new {@link Label} of this component
	 */
	public void rename(Label label);
	
	/**
	 * @return the {@link ComponentCollection} it is attached to
	 */
	public TComponentCollection getParentComponentCollection();
	
}
