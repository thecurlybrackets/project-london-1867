package de.thecurlybrackets.londonlib.parser.components;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;

import de.thecurlybrackets.londonlib.game.LondonWorld;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.Entity;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.components.Quest;
import de.thecurlybrackets.londonlib.game.worldobjects.places.Place;
import de.thecurlybrackets.londonlib.game.worldobjects.props.Prop;
import de.thecurlybrackets.londonlib.parser.exceptions.SessionPoolLoadElementException;

/**
 * Loads all used LondonMod-s and their LondonPart-s
 * into a centralized pool.
 * 
 * @author Tim Roethig
 * @see LondonMod
 * @see LondonPart
 */
public class SessionPool implements LondonPartComponentProvider, LondonModComponentProvider {
	protected HashMap<Label, LondonPart> parts 	= new HashMap<>();
	protected HashMap<Label, LondonMod> mods 	= new HashMap<>();
	
	/**
	 * Loading must be done via tailored methods
	 * and therefore only performs a basic initialization
	 */
	public SessionPool() {
		
	}
	
	/**
	 * Loads a LondonMod into the pool and provides it
	 * or provides a previously loaded LondonMod from the pool
	 * @param modIdentifier the identifier of the LondonMod
	 * @param parentWorld the {@link LondonWorld} requesting the {@link LondonMod} {@code modIdentifier}
	 * @return a previously loaded LondonMod if available else load it into the pool first
	 * @throws SessionPoolLoadElementException In case the mod has neither been previously loaded
	 * nor could be loaded
	 */
	public LondonMod provideLondonMod(Label modIdentifier, LondonWorld parentWorld) throws SessionPoolLoadElementException {
		LondonMod loadedMod = this.mods.get(modIdentifier);
		if(loadedMod != null) {
			return loadedMod;
		} else {
			try {
				loadedMod = new LondonMod(modIdentifier, this, parentWorld);
				this.mods.put(modIdentifier, loadedMod);
				return loadedMod;
			} catch (IllegalArgumentException | IOException | ParserConfigurationException e) {
				throw new SessionPoolLoadElementException(LondonMod.class, modIdentifier);
			}
		}
	}
	
	/**
	 * Loads a LondonPart into the pool and provides it
	 * or provides a previously loaded LondonPart from the pool.
	 * And adds the given parent to the loaded LondonPart
	 * @param partIdentifier The identifier of the LondonPart
	 * @param parent The LondonMod requesting the loading of the desired LondonPart
	 * @return A previously loaded LondonPart if available else load it into the pool first
	 * @throws SessionPoolLoadElementException In case the part has neither been previously loaded
	 * nor could be loaded
	 */
	public LondonPart provideLondonPart(Label partIdentifier, LondonMod parent) throws SessionPoolLoadElementException {
		LondonPart loadedPart = this.parts.get(partIdentifier);
		if(loadedPart != null) {
			loadedPart.addParent(parent);
			return loadedPart;
		} else {
			try {
				loadedPart = new LondonPart(partIdentifier, this);
				parent.attachComponentCollection(loadedPart);
				this.parts.put(partIdentifier, loadedPart);
				return loadedPart;
			} catch (IllegalArgumentException | IOException | ParserConfigurationException e) {
				throw new SessionPoolLoadElementException(LondonPart.class, partIdentifier);
			}
		}
	}
	
	/**
	 * Removes all LondonPart-s being not used by any LondonMod
	 */
	public void clearAbandonedParts() {
		this.parts.entrySet().removeIf((Entry<Label, LondonPart> partEntry) -> {
			//remove in case nothing points on this part
			return partEntry.getValue().getParentCount() == 0;
		});
	}
	
	/**
	 * Removes all LondonMod-s being not used by a LondonWorld
	 * and in turn 
	 */
	public void clearAbandonedMods() {
		this.mods.entrySet().removeIf((Entry<Label, LondonMod> modEntry) -> {
			//in case no world loads this mod
			if(modEntry.getValue().isLoadedByWorld()) {
				//remove this mod from its parts
				for(var part : modEntry.getValue().childrenParts.entrySet()) {
					part.getValue().removeParent(modEntry.getValue());
				}
				return true;
			}
			return false;
		});
		//remove all newly abandoned parts
		this.clearAbandonedParts();
	}
	
	public SessionPoolStatistics generateCurrentStats() {
		return new SessionPoolStatistics(this.parts.entrySet(), this.mods.entrySet());
	}

	@Override
	public Place getPlace(Label identifier, Label label) {
		return this.mods.get(identifier).getPlace(label);
	}



	@Override
	public Quest getQuest(Label identifier, Label label) {
		return this.mods.get(identifier).getQuest(label);
	}



	@Override
	public Entity getEntity(Label identifier, Label label) {
		return this.parts.get(identifier).getEntity(label);
	}



	@Override
	public Prop getProp(Label identifier, Label label) {
		return this.parts.get(identifier).getProp(label);
	}
	
}
