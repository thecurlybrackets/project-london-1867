package de.thecurlybrackets.londonlib.parser.components;

import de.thecurlybrackets.londonlib.game.worldobjects.Parent;

/**
 * Describes any Object loading
 * and using a ComponentCollection
 * being part of a SessionPool
 * 
 * @author Tim Roethig
 *
 * @param <TComponentCollection> The loaded ComponentCollection
 */
public interface ComponentCollectionLoader<TComponentCollection extends ComponentCollection> extends SessionPoolLoader,
										Parent<TComponentCollection> {
	
	/**
	 * Attaches a ComponentCollection to load from
	 * @param collection The attached ComponentCollection
	 */
	public void attachComponentCollection(TComponentCollection collection);
	
	/**
	 * Detaches a ComponentCollection as it is no longer used
	 * @param collection The detached ComponentCollection
	 */
	public void detachComponentCollection(TComponentCollection collection);
	
}
