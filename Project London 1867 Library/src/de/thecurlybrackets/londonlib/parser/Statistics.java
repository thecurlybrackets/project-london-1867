package de.thecurlybrackets.londonlib.parser;

import java.io.PrintStream;

/**
 * Anything providing statistical data
 * @author Tim Roethig
 *
 */
public interface Statistics {
	
	/**
	 * Prints all statistics to System.out
	 */
	public void printStats();
	
	/**
	 * Prints all statistics to a desired
	 * PrintStream
	 * @param stream The PrintStream for printing the statistics
	 */
	public void printStats(PrintStream stream);
	
}
