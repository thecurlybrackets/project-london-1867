package de.thecurlybrackets.londonlib.parser;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;

/**
 * Denotes the special case of StorableData using the XMLDataFormat
 * in order to provide tailored usage
 * 
 * @author Tim Roethig
 * 
 */
public abstract class StorableXMLData extends StorableData<XMLDataFormat> {

	public StorableXMLData(File loadFile, XMLDataFormat formattedData) throws IllegalArgumentException, IOException {
		super(loadFile, formattedData);
	}
	
	public Transformer getTransformer() {
		return this.formattedData.transformer;
	}
	
	public DocumentBuilder getDocumentBuilder() {
		return this.formattedData.builder;
	}

}
