package de.thecurlybrackets.londonlib.parser;

import java.io.File;
import java.io.IOException;

import de.thecurlybrackets.londonlib.parser.exceptions.SaveObjectToFileFailureException;

/**
 * Basic capabilities of storable data
 * 
 * @author Tim Roethig
 *
 */
public interface Storable {
	
	/**
	 * Returns the preferred file-extension of
	 * the file
	 * 
	 * @return The file-extension
	 */
	public abstract String getFileExtension();
	
	/**
	 * Saves all contents to the
	 * file from where they have been loaded
	 * @throws SaveObjectToFileFailureException on failure
	 */
	public void saveContents() throws SaveObjectToFileFailureException;
	
	/**
	 * Saves all contents
	 * of this instance to a file
	 * 
	 * @param file File for contents
	 * @throws SaveObjectToFileFailureException on failure
	 */
	public void saveContents(File file) throws SaveObjectToFileFailureException;
	
	/**
	 * Sets file as current file
	 * and loads its contents
	 * 
	 * @param file File with contents
	 * @throws IllegalArgumentException In case of an invalid file-format
	 * @throws IOException In case of reading-errors
	 */
	public void loadContents(File file) throws IllegalArgumentException, IOException;
	
	/**
	 * Reloads all contents from the currently set file
	 * @throws IllegalArgumentException Redirection
	 * @throws IOException Redirection
	 */
	public void reloadContents() throws IllegalArgumentException, IOException;
	
}
