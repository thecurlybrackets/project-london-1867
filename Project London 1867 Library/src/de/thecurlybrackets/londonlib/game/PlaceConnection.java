package de.thecurlybrackets.londonlib.game;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.worldobjects.ID;
import de.thecurlybrackets.londonlib.game.worldobjects.XMLContent;
import de.thecurlybrackets.londonlib.game.worldobjects.places.Place;
import de.thecurlybrackets.londonlib.parser.components.SessionPool;

/**
 * Represents a constant connection between two Place-s
 * as part of a parent-child-relation. Meant to be an element
 * in any kind of list in order to create a complex cluster of
 * connection whilst being linearly stored and thus easily iterable
 * @author Tim Roethig
 * @see LondonWorld
 */
public class PlaceConnection implements XMLContent {
	
	/**
	 * The {@link Place} this PlaceConection refers to
	 */
	protected Place ref;
	
	/* attribute-names */
	
	public static final String ATTRNAME_REF = "ref";
	
	/* node-names */
	public static final String NODENAME_PARENTS = "Parents";
	public static final String NODENAME_PARRENT = "Parent";
	
	/**
	 * Connects to Places with each other
	 * @param ref The affected Place
	 * @param parents the parents of the affected Place
	 */
	public PlaceConnection(Place ref, Place... parents) {
		this.ref = ref;
		for(var i : parents) {
			ref.addParent(i);
		}
	}
	
	/**
	 * Creates a PlaceConnection from an XML-Element
	 * @param element The XML-Element
	 * @param pool The SessionPool to load the places from
	 * @return The resulting PlaceConnection
	 */
	public static PlaceConnection constructFromXMLContents(Element element, SessionPool pool) {
		//from XML
		ID refID = new ID(element.getAttribute(ATTRNAME_REF));
		//there has to be one element only (exception otherwise)
		Element xmlParents = (Element)element.getElementsByTagName(NODENAME_PARENTS).item(0);
		
		//interpret
		Place ref = pool.getPlace(refID.getOwnerLabel(), refID.getLabel());
		//space for the converted parents
		Place[] parents = new Place[xmlParents.getChildNodes().getLength()];
		//convert parents
		for(int i = 0; i < parents.length; i++) {
			//abstract and check syntax
			ID curr = new ID(xmlParents.getChildNodes().item(i).getTextContent());
			//acquire Place
			parents[i] = pool.getPlace(curr.getOwnerLabel(), curr.getLabel());
		}
		PlaceConnection res = new PlaceConnection(ref, parents);
		return res;
	}
	
	@Override
	public Element createXMLContents(Document ofDoc) {
		Element main = ofDoc.createElement(this.getClass().getSimpleName());
		main.setAttribute(ATTRNAME_REF, this.ref.getID().toSimpleString());
		
		Element parents = ofDoc.createElement(NODENAME_PARENTS);
		for(var i : this.ref.getParents().values()) {
			Element parent = ofDoc.createElement(NODENAME_PARRENT);
			parent.setTextContent(i.getParentKey().toSimpleString());
		}
		
		main.appendChild(parents);
		
		return main;
	}
	
}
