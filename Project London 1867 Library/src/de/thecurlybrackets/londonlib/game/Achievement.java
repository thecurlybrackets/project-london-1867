package de.thecurlybrackets.londonlib.game;

/**
 * Achievements are optional goals for the player.
 * As they have no story, they are no Quests. The
 * progress made so far can be found in a logbook
 * 
 * @author Tim Roethig
 * @see de.thecurlybrackets.londonlib.game.worldobjects.props.Prop
 * 
 */
public class Achievement {

}
