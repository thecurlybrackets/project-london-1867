package de.thecurlybrackets.londonlib.game.exceptions;

public class UnknownEnumNameException extends Exception {

    private static final long serialVersionUID = -5360256659495278712L;

    public UnknownEnumNameException() {
        super("The given enum name is unknown");
    }

    public UnknownEnumNameException(Enum<?> unknownEnum) {
        super("The given enum name is unknown: " + unknownEnum.getDeclaringClass().getSimpleName() + "." + unknownEnum.name());
    }


}