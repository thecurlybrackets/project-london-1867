package de.thecurlybrackets.londonlib.game.exceptions;

public class ValueOutOfBoundsException extends Exception{

    private static final long serialVersionUID = 2088447342963790852L;

    public ValueOutOfBoundsException() {
        super("The given value is out of range");
    }

    public ValueOutOfBoundsException(String range) {
        super("The given value is out of range and should be in between " + range);
    }

}