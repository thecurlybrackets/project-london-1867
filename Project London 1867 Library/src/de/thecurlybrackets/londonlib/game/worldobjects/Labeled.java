package de.thecurlybrackets.londonlib.game.worldobjects;

/**
 * Anything which must contain a Label
 * for identification purposes
 * @author Tim Roethig
 * @see Label
 *
 */
public interface Labeled {
	
	/* attribute-names */
	/**
	 * XML-Attribute named 'label'
	 */
	public static final String ATTRNAME_LABEL = "label";
	
	/**
	* A relatively unique name for the very instance
	* using it
	* @return The Label
	*/
	public Label getLabel();
	
}
