/**
 * Contains {@link de.thecurlybrackets.londonlib.game.worldobjects.WorldObject} and
 * its components
 */
package de.thecurlybrackets.londonlib.game.worldobjects;