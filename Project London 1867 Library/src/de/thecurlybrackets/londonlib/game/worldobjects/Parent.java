package de.thecurlybrackets.londonlib.game.worldobjects;

import java.util.Map;

/**
 * Any class having {@link Child children}
 * and being stored inside of a {@link Map}
 * @author Tim Roethig
 *
 */
public interface Parent<TChildType extends Child> {
	
	/**
	 * Parents are stored inside of a {@link Map}
	 * and therefore must have an associated key
	 * @return the key to this Parent
	 */
	public Object getParentKey();
	
	/**
	 * Adds a {@link Child}
	 * @param child the {@link Child} to be added
	 */
	public void addChild(TChildType child);
	
	/**
	 * Removes a {@link Child}
	 * @param child the {@link Child} to be removed
	 */
	public default void removeChild(TChildType child) {
		this.getChildren().remove(child.getChildKey());
	}
	
	/**
	 * Wrapps {@link Map#get(Object)} using
	 * the {@link Map} {@link #getChildren()}
	 * @param key
	 * @return
	 */
	public default TChildType getChild(Object key) {
		return this.getChildren().get(key);
	}
	
	/**
	 * 
	 * @return all children of this Parent
	 */
	public Map<?, TChildType> getChildren();
	
}
