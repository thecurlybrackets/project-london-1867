package de.thecurlybrackets.londonlib.game.worldobjects;

import java.util.Map;

/**
 * Any class having {@link Parent parents}
 * and being stored inside of a {@link Map}
 * @author Tim Roethig
 *
 */
public interface Child {
	
	/**
	 * Children are stored inside of a {@link Map}
	 * and therefore must have an associated key
	 * @return the key to this Child
	 */
	public Object getChildKey();
	
}
