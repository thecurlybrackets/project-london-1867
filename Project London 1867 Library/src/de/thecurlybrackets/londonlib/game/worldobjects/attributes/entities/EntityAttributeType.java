package de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities;

public enum EntityAttributeType {
    HEALTH, LOAD_CAPACITY, MONEY, HUMANENESS, REPUTATION
}