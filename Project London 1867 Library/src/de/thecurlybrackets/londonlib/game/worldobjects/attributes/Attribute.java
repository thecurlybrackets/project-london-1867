package de.thecurlybrackets.londonlib.game.worldobjects.attributes;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.UnknownEnumNameException;
import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;
import de.thecurlybrackets.londonlib.game.worldobjects.XMLContent;

public abstract class Attribute implements XMLContent {

    protected float value;
    protected float minValue;
    protected float maxValue;
    protected float standardValue;

    public static final String ATTRNAME_VALUE = "value";
    public static final String ATTRNAME_MIN_VALUE = "minValue";
    public static final String ATTRNAME_MAX_VALUE = "maxValue";
    public static final String ATTRNAME_STD_VALUE = "standardValue";
    
    //constructor
    
    public Attribute(float maxValue) throws ValueOutOfBoundsException {
        this(0, maxValue, 0, 0);
    }

    public Attribute(float minValue, float maxValue, float value) throws ValueOutOfBoundsException {
        this(minValue, maxValue, value, value);
    }

    public Attribute(float minValue, float maxValue, float value, float standardValue) throws ValueOutOfBoundsException {
    	if(minValue > maxValue) {
            throw new ValueOutOfBoundsException();
        }

        if(value < minValue || value > maxValue || standardValue < minValue || standardValue > maxValue) {
            throw new ValueOutOfBoundsException(minValue + " and " + maxValue);
        } else {
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.value = value;
            this.standardValue = standardValue;
        }
    }

    //methods for working with 'value'
    
    public float get() {
        return value;
    }

    public float getAsPercentage() {
        return value / ((maxValue - minValue) / 100);
    }

    public boolean set(float value) {
        if(value < minValue || value > maxValue) {
            return false;
        }
        this.value = value;
        return true;
    }

    public boolean plus(float amount) {
        if(value + amount > maxValue) {
            return false;
        }
        value += amount;
        return true;
    }

    public boolean minus (float amount) {
        if(value - amount < minValue) {
            return false;
        }
        value -= amount;
        return true;
    }

    //methods for working with other basic-condition-attributes

    public float getValueOf(AttributeProperty variableName) throws UnknownEnumNameException {
        switch (variableName) {
            case MIN_VALUE:
                return this.minValue;
            case MAX_VALUE:
                return this.maxValue;
            case STANDARD_VALUE:
                return this.standardValue;
            case VALUE:
                return get();
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }

    public boolean seValueOf(AttributeProperty variableName, float value) throws UnknownEnumNameException {
        switch (variableName) {
            case MIN_VALUE:
                if(minValue > value || minValue > standardValue) {
                    return false;
                }
                this.minValue = value;
                return true;
            case MAX_VALUE:
            if(maxValue < value || maxValue < standardValue) {
                return false;
            }
            this.maxValue = value;
            return true;
            case STANDARD_VALUE:
                if(standardValue < minValue || standardValue > maxValue) {
                    return false;
                }
                this.standardValue = value;
                return true;
            case VALUE:
                return set(value);
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }

    public boolean plusValueOf(AttributeProperty variableName, float amount) throws UnknownEnumNameException {
        switch (variableName) {
            case MIN_VALUE:
                if(minValue + amount > value || minValue + amount > standardValue) {
                    return false;
                }
                this.minValue += amount;
                return true;
            case MAX_VALUE:
                this.maxValue += amount;
                return true;
            case STANDARD_VALUE:
                if(standardValue + amount > maxValue) {
                    return false;
                }
                this.standardValue += amount;
                return true;
            case VALUE:
                return plus(amount);
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }

    public boolean minusValueOf(AttributeProperty variableName, float amount) throws UnknownEnumNameException {
        switch (variableName) {
            case MIN_VALUE:
                this.minValue -= amount;
                return true;
            case MAX_VALUE:
                if(maxValue - amount < value || maxValue + amount < standardValue) {
                    return false;
                }
                this.maxValue -= amount;
                return true;
            case STANDARD_VALUE:
                if(standardValue - amount < minValue) {
                    return false;
                }
                this.standardValue += amount;
                return true;
            case VALUE:
                return minus(amount);
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }
    
    @Override
    public Element createXMLContents(Document ofDoc) {
    	Element main = ofDoc.createElement(this.getClass().getSimpleName());
    	//attribute "value"
    	main.setAttribute(ATTRNAME_VALUE, String.valueOf(this.value));
    	//attribute "minValue"
    	main.setAttribute(ATTRNAME_MIN_VALUE, String.valueOf(this.minValue));
    	//attribute "maxValue"
    	main.setAttribute(ATTRNAME_MAX_VALUE, String.valueOf(this.maxValue));
    	//attribute "standardValue"
    	main.setAttribute(ATTRNAME_STD_VALUE, String.valueOf(this.standardValue));
    	
    	return main;
    }
    
}
