package de.thecurlybrackets.londonlib.game.worldobjects.attributes;

public enum AttributeProperty {
	VALUE, MIN_VALUE, MAX_VALUE, STANDARD_VALUE
}
