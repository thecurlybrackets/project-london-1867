package de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities;

import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.Attribute;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.AttributeXMLExtract;

public class Health extends Attribute {
	
	public static Health constructFromXMLContents(Element contents) throws ValueOutOfBoundsException {
		var extract = AttributeXMLExtract.constructFromXMLContents(contents);
		return new Health(extract.getMinValue(), extract.getMaxValue(), extract.getValue(), extract.getStandardValue());
	}
	
    public Health(float maxValue) throws ValueOutOfBoundsException {
        super(maxValue);
    }

    public Health(float minValue, float maxValue, float value) throws ValueOutOfBoundsException {
        super(minValue, maxValue, value, value);
    }

    public Health(float minValue, float maxValue, float value, float standardValue) throws ValueOutOfBoundsException {
        super(minValue, maxValue, value, standardValue);
    }
	
}
