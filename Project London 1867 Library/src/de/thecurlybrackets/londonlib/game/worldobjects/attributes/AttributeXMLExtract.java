package de.thecurlybrackets.londonlib.game.worldobjects.attributes;

import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;

/**
 * Structure containing extracted data
 * of an {@link Attribute}
 * @author Tim Roethig
 *
 */
public class AttributeXMLExtract {
	
	protected float value;
    protected float minValue;
    protected float maxValue;
    protected float standardValue;
	
    /**
	 * Extracts all {@link Attribute}-data from
	 * an XML-Element and stores it as its XMLExtract
	 * @param elem the XML-contents
	 * @return the extracted data
	 */
    public static AttributeXMLExtract constructFromXMLContents(Element elem) throws ValueOutOfBoundsException {
		var xmlMinValue = elem.getAttribute(Attribute.ATTRNAME_MIN_VALUE);
		var xmlMaxValue = elem.getAttribute(Attribute.ATTRNAME_MAX_VALUE);
		var xmlValue = elem.getAttribute(Attribute.ATTRNAME_VALUE);
		var xmlStandardValue = elem.getAttribute(Attribute.ATTRNAME_STD_VALUE);
		
		float minValue = Float.parseFloat(xmlMinValue);
		float maxValue = Float.parseFloat(xmlMaxValue);
		float value = Float.parseFloat(xmlValue);
		float standardValue = Float.parseFloat(xmlStandardValue);
		
		return new AttributeXMLExtract(minValue, maxValue, value, standardValue);
	}
    
    /**
     * Merges all extracted data at once
     * @param minValue Extracted minValue
     * @param maxValue Extracted maxValue
     * @param value Extracted value
     * @param standardValue Extracted standardValue
     * @throws ValueOutOfBoundsException
     */
	public AttributeXMLExtract(float minValue, float maxValue, float value, float standardValue)
			throws ValueOutOfBoundsException {
		if(minValue > maxValue) {
            throw new ValueOutOfBoundsException();
        }

        if(value < minValue || value > maxValue || standardValue < minValue || standardValue > maxValue) {
            throw new ValueOutOfBoundsException(minValue + " and " + maxValue);
        } else {
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.value = value;
            this.standardValue = standardValue;
        }
	}
	
	public float getMinValue() {
		return this.minValue;
	}
	
	public float getMaxValue() {
		return this.maxValue;
	}
	
	public float getValue() {
		return this.value;
	}
	
	public float getStandardValue() {
		return this.standardValue;
	}
	
}
