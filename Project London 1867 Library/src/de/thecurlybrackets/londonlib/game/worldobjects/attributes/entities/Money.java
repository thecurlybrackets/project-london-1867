package de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities;

import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.Attribute;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.AttributeXMLExtract;

public class Money extends Attribute {
	
	public static Money constructFromXMLContents(Element contents) throws ValueOutOfBoundsException {
		var extract = AttributeXMLExtract.constructFromXMLContents(contents);
		return new Money(extract.getMinValue(), extract.getMaxValue(), extract.getValue(), extract.getStandardValue());
	}
	
    public Money(float maxValue) throws ValueOutOfBoundsException {
        super(maxValue);
    }

    public Money(float minValue, float maxValue, float value) throws ValueOutOfBoundsException {
        super(minValue, maxValue, value, value);
    }

    public Money(float minValue, float maxValue, float value, float standardValue) throws ValueOutOfBoundsException {
        super(minValue, maxValue, value, standardValue);
    }
 
}