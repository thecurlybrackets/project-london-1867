package de.thecurlybrackets.londonlib.game.worldobjects.attributes.props;

import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.Attribute;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.AttributeXMLExtract;

public class Weight extends Attribute {
	
	public static Weight constructFromXMLContents(Element contents) throws ValueOutOfBoundsException {
		var extract = AttributeXMLExtract.constructFromXMLContents(contents);
		return new Weight(extract.getMinValue(), extract.getMaxValue(), extract.getValue(), extract.getStandardValue());
	}
	
    public Weight(float maxValue) throws ValueOutOfBoundsException {
        super(maxValue);
    }

    public Weight(float minValue, float maxValue, float value) throws ValueOutOfBoundsException {
        super(minValue, maxValue, value, value);
    }

    public Weight(float minValue, float maxValue, float value, float standardValue) throws ValueOutOfBoundsException {
        super(minValue, maxValue, value, standardValue);
    }

}