package de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities;

import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.Attribute;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.AttributeXMLExtract;

public class LoadCapacity extends Attribute {
	
	public static LoadCapacity constructFromXMLContents(Element contents) throws ValueOutOfBoundsException {
		var extract = AttributeXMLExtract.constructFromXMLContents(contents);
		return new LoadCapacity(extract.getMinValue(), extract.getMaxValue(), extract.getValue(), extract.getStandardValue());
	}
	
    public LoadCapacity(float maxValue) throws ValueOutOfBoundsException {
        super(maxValue);
    }

    public LoadCapacity(float minValue, float maxValue, float value) throws ValueOutOfBoundsException {
        super(minValue, maxValue, value, value);
    }

    public LoadCapacity(float minValue, float maxValue, float value, float standardValue) throws ValueOutOfBoundsException {
        super(minValue, maxValue, value, standardValue);
    }

}