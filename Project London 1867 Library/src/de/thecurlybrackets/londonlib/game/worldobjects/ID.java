package de.thecurlybrackets.londonlib.game.worldobjects;

import de.thecurlybrackets.londonlib.parser.components.LondonMod;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

/**
 * This is unique data designed
 * for the unambiguous identification
 * of Identifiable-s. It is path-like
 * structured using various patterns
 * 
 * @author Tim Roethig
 * @see Label
 * @see Term
 * @see Identifiable
 */
public final class ID implements Labeled {

	/**
	 * @see #getTerm()
	 */
	private Term term;
	
	/**
	 * @see #getOwnerLabel()
	 */
	private Label ownerLabel;
	
	/**
	 * @see #ownerSuffix
	 */
	private Label ownerSuffix;
	
	/**
	 * @see #getLabel()
	 */
	private Label label;
	
	/**
	 * Comparable String equivalent of {@link #PART_SUFFIX}
	 * (used in {@link #ID(String)})
	 */
	private static final String PART_SUFFIX_COMP = "part";
	
	/**
	 * Comparable String equivalent of {@link #MOD_SUFFIX}
	 * (used in {@link #ID(String)})
	 */
	private static final String MOD_SUFFIX_COMP = "mod";
	
	/**
	 * Denotes an owner as LondonPart
	 * @see LondonPart
	 */
	public static final Label PART_SUFFIX = new Label(LondonPart.PART_EXTENSION);
	
	/**
	 * Denotes an owner as LondonMod
	 * @see LondonMod
	 */
	public static final Label MOD_SUFFIX = new Label(LondonMod.MOD_EXTENSION);
	
	/**
	 * The character used for separating the Labels
	 */
	public static final String FIELD_SEPARATOR = ":";
	
	/**
	 * Identifies an owner-independent
	 * object such as
	 * {@link de.thecurlybrackets.londonlib.game.worldobjects.entities.Player Player} as it
	 * is uniquely assigned to a world
	 * @param term Term
	 * @param label Label
	 */
	public ID(Term term, Label label) {
		if(term == null || label == null) {
			throw new NullPointerException("");
		}
		this.term = term;
		this.label = label;
	}
	
	/**
	 * Synonym
	 * @see #ID(Term, Label)
	 * @param term Term
	 * @param label Label from String
	 */
	public ID(Term term, String label) {
		this(term, new Label(label));
	}
	
	/**
	 * Identifies a {@link de.thecurlybrackets.londonlib.parser.components.Component Component}
	 * @param term Term
	 * @param owner The owning LondonMod
	 * @param label Label
	 */
	public ID(Term term, LondonMod owner, Label label) {
		this.term = term;
		this.ownerLabel = owner.getLabel();
		this.ownerSuffix = MOD_SUFFIX;
		this.label = label;
	}
	
	/**
	 * Synonym
	 * @see #ID(Term, LondonMod, Label)
	 * @param term Term
	 * @param owner The owning LondonMod
	 * @param label Label
	 */
	public ID(Term term, LondonMod owner, String label) {
		this(term, owner, new Label(label));
	}
	
	/**
	 * Identifies a {@link de.thecurlybrackets.londonlib.parser.components.Component Component}
	 * @param term Term
	 * @param owner The owning LondonPart
	 * @param label Label
	 */
	public ID(Term term, LondonPart owner, Label label) {
		this.term = term;
		this.ownerLabel = owner.getLabel();
		this.ownerSuffix = PART_SUFFIX;
		this.label = label;
	}
	
	/**
	 * Synonym
	 * @see #ID(Term, LondonPart, Label)
	 * @param term Term
	 * @param owner The owning LondonPart
	 * @param label Label
	 */
	public ID(Term term, LondonPart owner, String label) {
		this(term, owner, new Label(label));
	}
	
	/**
	 * Creates an ID from an ID previously made to a String
	 * @param parsedID String of patter {@link #toString()} or {@link #toSimpleString()}
	 * @throws IllegalArgumentException In case of an incorrectly formatted String
	 */
	public ID(String parsedID) throws IllegalArgumentException {
		//refers to the end of the Term
		int brackIndex = parsedID.indexOf(')');
		//in case there are no brackets
		if(brackIndex == -1) {
			throw new IllegalArgumentException(ExceptionMessages.makeIDParsingErrorMessage(parsedID));
		}
		//extracting the Term
		this.term = new Term(parsedID.substring(0, brackIndex));
		//getting the label AND the owner's label+suffix
		String[] labels = parsedID.split(FIELD_SEPARATOR);
		//if both this' and owner's label are available
		if(labels.length == 2) {
			//separating owner's label from suffix
			String[] ownerIdentifierSpecs = labels[0].split(Term.TERM_COMPONENT_SEPARATOR);
			//only continue if both are potentially available
			if(ownerIdentifierSpecs.length == 2) {
				this.ownerLabel		= new Label(ownerIdentifierSpecs[0]);
				//verify suffix
				switch(ownerIdentifierSpecs[1]) {
				case PART_SUFFIX_COMP:
					this.ownerSuffix = PART_SUFFIX;
					break;
				case MOD_SUFFIX_COMP:
					this.ownerSuffix = MOD_SUFFIX;
					break;
				default:
					throw new IllegalArgumentException(ExceptionMessages.makeIDParsingErrorMessage(parsedID));
				}
			} else {
				throw new IllegalArgumentException(ExceptionMessages.makeIDParsingErrorMessage(parsedID));
			}
			//in case of a collection-independent ID
		} else if(labels.length == 1) {
			this.term 	= new Term(labels[0]);
			this.label	= new Label(labels[1]);
		} else {
			throw new IllegalArgumentException(ExceptionMessages.makeIDParsingErrorMessage(parsedID));
		}
	}
	
	/**
	 * Transforms its contents to a parsable String
	 * using the pattern<br>
	 * (TERM){@link #toSimpleString()}
	 */
	@Override
	public String toString() {
		return this.term.getTextWithBrackets() + this.toSimpleString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ID) {
			ID id = (ID)obj;
			//equals if all attributes are equals
			return 	this.term.equals(id.term) &&
					this.label.equals(id.label) &&
					this.ownerLabel.equals(id.ownerLabel) &&
					this.ownerSuffix.equals(id.ownerSuffix);
		}
		return false;
	}
	
	/**
	 * Transforms its contents to a parsable String
	 * using the pattern<br>
	 * LABEL:LABEL_OF_OWNER.SUFFIX_OF_OWNER<br>
	 * or<br>
	 * LABEL
	 */
	public String toSimpleString() {
		if(this.ownerLabel != null) {
			return this.label + FIELD_SEPARATOR + this.ownerLabel + this.ownerSuffix;
		} else {
			return this.label.getText();
		}
	}
	
	/**
	* The Term of the object
	* (e.g.: Item OR Dialogue)
	*/
	public Term getTerm() {
		return this.term;
	}
	
	/**
	* Label of the owner
	*/
	public Label getOwnerLabel() {
		return this.ownerLabel;
	}
	
	/**
	* Specifies the type of the owner
	* @see #PART_SUFFIX
	* @see #MOD_SUFFIX
	*/
	public Label getOwnerSuffix() {
		return this.ownerSuffix;
	}
	
	
	@Override
	public Label getLabel() {
		return this.label;
	}
	
}
