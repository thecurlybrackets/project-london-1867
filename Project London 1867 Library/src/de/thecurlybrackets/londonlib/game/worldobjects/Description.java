package de.thecurlybrackets.londonlib.game.worldobjects;

import org.w3c.dom.Element;

/**
 * Adds written details to
 * WorldObject-s
 * 
 * @author Tim Roethig
 * @see de.thecurlybrackets.londonlib.game.worldobjects.WorldObject
 *
 */
public class Description extends Text {
	
	/* node-names */
	/**
	 * XML-Node-Name of Descriptions
	 */
	public static final String NODENAME_XMLNAME = Description.class.getSimpleName();
	
	/**
	 * Constructs a Description from XML-Contents
	 * @param elem XML-Representation of any Description
	 * @return
	 */
	public static Description constructFromXMLContents(Element elem) {
		return new Description(Text.constructFromXMLContents(elem).getText());
	}
	
	/**
	 * Creates an empty Description
	 */
	public Description() {
		super();
	}
	
	/**
	 * Creates a Description with the
	 * given text
	 * @param descr the text of the Description
	 */
	public Description(String descr) {
		super(descr);
	}
	
}
