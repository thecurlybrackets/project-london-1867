package de.thecurlybrackets.londonlib.game.worldobjects;

/**
 * Denotes a {@link Child} having one {@link Parent}
 * @author Tim Roethig
 *
 * @param <TParentType> type of the {@link Parent}
 */
public interface SingleParented<TParentType extends Parent<?>> extends Child {
	
	/**
	 * Sets the current parent
	 * @param parent
	 */
	public void setParent(TParentType parent);
	
	/**
	 * Sets the current parent to {@code null}
	 */
	public default void unsetParent() {
		this.setParent(null);
		return;
	}
	
	/**
	 * 
	 * @return the current parent
	 */
	public TParentType getParent();
	
}
