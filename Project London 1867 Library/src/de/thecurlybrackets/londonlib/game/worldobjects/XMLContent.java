package de.thecurlybrackets.londonlib.game.worldobjects;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Creates to-be-permanently-accessible data in
 * an XML-Node, thus data
 * can also be loaded and interpreted from it again
 * 
 * @author Tim Roethig
 *
 */
public interface XMLContent {
	
	/**
	 * Makes a yet not stored XML-Element out of an object's contents,
	 * so it can be modified by child-classes
	 * @return The resulting XML-Element
	 */
	public Element createXMLContents(Document ofDoc);
	
}
