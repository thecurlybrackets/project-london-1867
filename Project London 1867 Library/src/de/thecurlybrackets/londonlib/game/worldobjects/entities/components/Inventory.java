package de.thecurlybrackets.londonlib.game.worldobjects.entities.components;

import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.UnknownEnumNameException;
import de.thecurlybrackets.londonlib.game.worldobjects.ID;
import de.thecurlybrackets.londonlib.game.worldobjects.XMLContent;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.EntityAttributeType;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.Entity;
import de.thecurlybrackets.londonlib.game.worldobjects.props.items.Item;
import de.thecurlybrackets.londonlib.game.worldobjects.props.items.ItemStack;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

public final class Inventory implements XMLContent {

    private Entity owner;
    private HashMap<ID, ItemStack> itemStacks = new HashMap<>();
    private float weight;
    
    /* node-names */
    public static final String NODENAME_XMLNAME = Inventory.class.getSimpleName();
    public static final String NODENAME_ITEMSTACKS = "ItemStacks";
    
    public static Inventory constructFromXMLContents(Element node, LondonPart londonPart) {
		Inventory inventory = new Inventory();
		//all itemStacks as list
		var xmlItemStacks = node.getElementsByTagName(NODENAME_ITEMSTACKS).item(0).getChildNodes();
		//construct ItemStacks
		for(int i = 0; i < xmlItemStacks.getLength(); i++) {
			ItemStack itemStack = ItemStack.constructFromXMLContents((Element)xmlItemStacks.item(0), londonPart);
			inventory.itemStacks.put(itemStack.getItemTypeID(), itemStack);
		}
		return inventory;
	}
    
    public Inventory() {
        this.owner = null;
        weight = 0;
    }
    
    public void setOwner(Entity owner) {
    	this.owner = owner;
    	return;
    }

    public boolean addItem(Item item) throws UnknownEnumNameException {
        if(weight + item.getWeight() > owner.get(EntityAttributeType.LOAD_CAPACITY)) {
            return false;
        }
        
        if(itemStacks.get(item.getID()) == null) {
            ItemStack itemStack = new ItemStack(item, item.getStackability());
            itemStack.pushNew();
            itemStacks.put(item.getID(), itemStack);
        } else {
        	itemStacks.get(item.getID()).pushNew();
        }
        
        weight += item.getWeight();
        return true;
    }

    public Item getItem(ID key) {
        if(itemStacks.get(key) == null) {
            return null;
        }
        
        return itemStacks.get(key).peek();
    }

    public Item destroyItem(ID key) {
        if(itemStacks.get(key) == null) {
            return null;
        }
        
        return itemStacks.get(key).poll();
    }

    public void clearInventory() {
    	itemStacks.clear();
        weight = 0;
    }

	@Override
	public Element createXMLContents(Document ofDoc) {
		Element main = ofDoc.createElement(NODENAME_XMLNAME);
		Element itemStacks = ofDoc.createElement(NODENAME_ITEMSTACKS);
		for(var i : this.itemStacks.values()) {
			itemStacks.appendChild(i.createXMLContents(ofDoc));
		}
		main.appendChild(itemStacks);
		
		return main;
	}

}