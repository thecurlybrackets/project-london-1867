package de.thecurlybrackets.londonlib.game.worldobjects.entities;

import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.Health;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.LoadCapacity;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.components.Inventory;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

/**
 * Structure containing extracted data
 * of a {@link Entity}
 * @author Tim Roethig
 *
 */
public class EntityXMLExtract {
	
    protected Inventory inventory;
    protected Health health;
    protected LoadCapacity loadCapacity;
	
    /**
     * 
     * Extracts all {@link Entity}-data from
	 * an XML-Element and stores it as its XMLExtract
	 * @param elem the XML-contents
	 * @return the extracted data
     */
    public static EntityXMLExtract constructFromXMLContents(Element elem, LondonPart parentPart) throws ValueOutOfBoundsException {
    	//separating
    	var xmlInventory = (Element)elem.getElementsByTagName(Inventory.class.getSimpleName()).item(0);
    	var xmlHealth = (Element)elem.getElementsByTagName(Health.class.getSimpleName()).item(0);
    	var xmlLoadCapacity = (Element)elem.getElementsByTagName(LoadCapacity.class.getSimpleName()).item(0);
    	
    	Inventory inventory = Inventory.constructFromXMLContents(xmlInventory, parentPart);
    	Health health = Health.constructFromXMLContents(xmlHealth);
    	LoadCapacity loadCapacity = LoadCapacity.constructFromXMLContents(xmlLoadCapacity);
    	
    	return new EntityXMLExtract(inventory, health, loadCapacity);
    }
    
    /**
     * Merges all extracted data at once
     * @param inventory Extracted Inventory
     * @param health Extracted Health
     * @param loadCapacity Extracted LoadCapacity
     */
    public EntityXMLExtract(Inventory inventory, Health health, LoadCapacity loadCapacity) {
    	this.inventory = inventory;
    	this.health = health;
    	this.loadCapacity = loadCapacity;
    }
    
    public Inventory getInventory() {
    	return this.inventory;
    }
    
    public Health getHealth() {
    	return this.health;
    }
    
    public LoadCapacity getLoadCapacity() {
    	return this.loadCapacity;
    }
    
}
