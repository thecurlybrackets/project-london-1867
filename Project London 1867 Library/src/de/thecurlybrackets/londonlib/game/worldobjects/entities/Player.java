package de.thecurlybrackets.londonlib.game.worldobjects.entities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.LondonWorld;
import de.thecurlybrackets.londonlib.game.worldobjects.Description;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.Name;
import de.thecurlybrackets.londonlib.game.worldobjects.Term;

public class Player extends Person {
	protected LondonWorld currentWorld;
	
	public static final Term TERM = new Term(Player.class.getSimpleName());
	public static final Label LABEL = new Label("player");
	
	public Player(LondonWorld world) {
        super(LABEL);
    }
	
    public Player(LondonWorld world, Name name, Description description) {
        super(LABEL, null, name, description);
        this.currentWorld = world;
    }
    
    @Override
    public Term getTerm() {
    	return TERM;
    }
    
    @Override
    public Element createXMLContents(Document ofDoc) {
    	Element main = super.createXMLContents(ofDoc);
    	ofDoc.renameNode(main, null, TERM.getText());
    	
    	
    	return main;
    }
    
}