package de.thecurlybrackets.londonlib.game.worldobjects.entities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.UnknownEnumNameException;
import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;
import de.thecurlybrackets.londonlib.game.worldobjects.Description;
import de.thecurlybrackets.londonlib.game.worldobjects.ID;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.Name;
import de.thecurlybrackets.londonlib.game.worldobjects.SingleParented;
import de.thecurlybrackets.londonlib.game.worldobjects.Term;
import de.thecurlybrackets.londonlib.game.worldobjects.WorldObject;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.EntityAttributeType;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.Health;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.LoadCapacity;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.components.Inventory;
import de.thecurlybrackets.londonlib.game.worldobjects.places.Place;
import de.thecurlybrackets.londonlib.parser.components.Component;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

public abstract class Entity extends WorldObject implements Component<LondonPart>, SingleParented<Place> {

	protected LondonPart parentPart;
    protected Inventory inventory;
    protected Health health;
    protected LoadCapacity loadCapacity;
    protected Place parentPlace;
    
    public static final Term TERM = new Term("Entity");
    
    public static final Float MAX_HEALTH = 100f;
    public static final Float MAX_LOAD_CAPACITY = 100f;
	
    protected Entity(Label label) {
    	super(label);
    	this.inventory = new Inventory();
		try {
    		this.health = new Health(MAX_HEALTH);
        	this.loadCapacity = new LoadCapacity(MAX_LOAD_CAPACITY);
    	} catch(ValueOutOfBoundsException e) {
    		e.printStackTrace();
    	}
    }
    
    protected Entity(String label) {
    	this(new Label(label));
    }
    
	public Entity(Label label, LondonPart parentPart) {
		super(label, parentPart);
		parentPart.addElement(this);
		this.inventory = new Inventory();
		try {
    		this.health = new Health(MAX_HEALTH);
        	this.loadCapacity = new LoadCapacity(MAX_LOAD_CAPACITY);
    	} catch(ValueOutOfBoundsException e) {
    		e.printStackTrace();
    	}
	}
	
	public Entity(String label, LondonPart parentPart) {
		this(new Label(label), parentPart);
	}
    
    public Entity(Label label, LondonPart parentPart, Name name, Description description) {
        super(label, parentPart, name, description);
        parentPart.addElement(this);
        try {
			this.init(new Inventory(), new Health(MAX_HEALTH), new LoadCapacity(MAX_LOAD_CAPACITY));
		} catch (ValueOutOfBoundsException e) {
			e.printStackTrace();
		}
    }
    
    public Entity(String label, LondonPart parentPart, Name name, Description description) {
        this(new Label(label), parentPart, name, description);
    }

    //must be called before other methods
    protected void init(Inventory inventory, Health health, LoadCapacity loadCapacity) {
        this.inventory = inventory;
        this.inventory.setOwner(this);
        this.health = health;
        this.loadCapacity = loadCapacity;
    }

    public float get(EntityAttributeType variableName) throws UnknownEnumNameException {
        switch (variableName) {
            case HEALTH:
                return health.get();
            case LOAD_CAPACITY:
                return loadCapacity.get();
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }

    public float getAsPercentage(EntityAttributeType variableName) throws UnknownEnumNameException {
        switch (variableName) {
            case HEALTH:
                return health.getAsPercentage();
            case LOAD_CAPACITY:
                return loadCapacity.getAsPercentage();
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }

    public boolean set(EntityAttributeType variableName, float value) throws UnknownEnumNameException {
        switch (variableName) {
            case HEALTH:
                return health.set(value);
            case LOAD_CAPACITY:
                return loadCapacity.set(value);
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }

    public boolean plus(EntityAttributeType variableName, float amount) throws UnknownEnumNameException {
        switch (variableName) {
            case HEALTH:
                return health.plus(amount);
            case LOAD_CAPACITY:
                return loadCapacity.plus(amount);
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }

    public boolean minus(EntityAttributeType variableName, float amount) throws UnknownEnumNameException {
        switch (variableName) {
            case HEALTH:
                return health.minus(amount);
            case LOAD_CAPACITY:
                return loadCapacity.minus(amount);
            default:
                throw new UnknownEnumNameException(variableName);
        }
    }
    
    @Override
	public void bindTo(Label label, LondonPart destination) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void moveTo(LondonPart destination) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void rename(Label label) {
		//TODO
	}
    
    @Override
    public LondonPart getParentComponentCollection() {
    	return this.parentPart;
    }
    
    @Override
    public Element createXMLContents(Document ofDoc) {
    	Element main = super.createXMLContents(ofDoc);
		//adding inventory
		main.appendChild(this.inventory.createXMLContents(ofDoc));
		main.appendChild(this.health.createXMLContents(ofDoc));
		main.appendChild(this.loadCapacity.createXMLContents(ofDoc));
		return main;
    }
    
    @Override
    public Term getTerm() {
    	return TERM;
    }
    
	@Override
	public ID getChildKey() {
		return this.getID();
	}

	@Override
	public void setParent(Place parent) {
		this.parentPlace = parent;
	}

	@Override
	public Place getParent() {
		return this.parentPlace;
	}

}
