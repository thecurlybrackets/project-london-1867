package de.thecurlybrackets.londonlib.game.worldobjects.entities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.exceptions.UnknownEnumNameException;
import de.thecurlybrackets.londonlib.game.exceptions.ValueOutOfBoundsException;
import de.thecurlybrackets.londonlib.game.worldobjects.Description;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.Name;
import de.thecurlybrackets.londonlib.game.worldobjects.Term;
import de.thecurlybrackets.londonlib.game.worldobjects.WorldObjectXMLExtract;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.EntityAttributeType;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.Health;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.Humaneness;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.LoadCapacity;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.Money;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.entities.Reputation;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.components.Inventory;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

public class Person extends Entity {

    protected Money money;
    protected Humaneness humaneness;
    protected Reputation reputation;
    
    public static final Float MAX_MONEY = 99999.0f;
    public static final Float MAX_HUMANENESS = 100f;
    public static final Float MAX_REPUTATION = 100f;
    
    public static final Term TERM = new Term(Person.class.getSimpleName());
    
    public static Person constructFromXMLContents(Element node, LondonPart parentPart) throws ValueOutOfBoundsException {
		EntityXMLExtract eExtract = EntityXMLExtract.constructFromXMLContents(node, parentPart);
		WorldObjectXMLExtract woExtract = WorldObjectXMLExtract.constructFromXMLContents(node);
		Person result = new Person(woExtract.getLabel(), parentPart, woExtract.getName(), woExtract.getDescription());
		eExtract.getInventory().setOwner(result);
		var xmlMoney = (Element)node.getElementsByTagName(Money.class.getSimpleName()).item(0);
		var xmlHumaneness = (Element)node.getElementsByTagName(Humaneness.class.getSimpleName()).item(0);
		var xmlReputation = (Element)node.getElementsByTagName(Reputation.class.getSimpleName()).item(0);
		
		Money money = Money.constructFromXMLContents(xmlMoney);
		Humaneness humaneness = Humaneness.constructFromXMLContents(xmlHumaneness);
		Reputation reputation = Reputation.constructFromXMLContents(xmlReputation);
		
		result.init(eExtract.getInventory(), eExtract.getHealth(), eExtract.getLoadCapacity(), money, humaneness, reputation);
		return result;
	}
    
    protected Person(Label label) {
    	super(label);
    	this.init(new Inventory(), MAX_HEALTH, MAX_LOAD_CAPACITY, MAX_MONEY, MAX_HUMANENESS, MAX_REPUTATION);
    }
    
    protected Person(String label) {
    	super(label);
    	this.init(new Inventory(), MAX_HEALTH, MAX_LOAD_CAPACITY, MAX_MONEY, MAX_HUMANENESS, MAX_REPUTATION);
    }
    
    public Person(Label label, LondonPart parentPart) {
        super(label, parentPart);
        this.init(new Inventory(), MAX_HEALTH, MAX_LOAD_CAPACITY, MAX_MONEY, MAX_HUMANENESS, MAX_REPUTATION);
    }
    
    public Person(String label, LondonPart parentPart) {
        super(label, parentPart);
        this.init(new Inventory(), MAX_HEALTH, MAX_LOAD_CAPACITY, MAX_MONEY, MAX_HUMANENESS, MAX_REPUTATION);
    }

    public Person(Label label, LondonPart parentPart, Name name, Description description) {
        super(label, parentPart, name, description);
        this.init(new Inventory(), MAX_HEALTH, MAX_LOAD_CAPACITY, MAX_MONEY, MAX_HUMANENESS, MAX_REPUTATION);
    }
    
    public Person(String label, LondonPart parentPart, Name name, Description description) {
        super(label, parentPart, name, description);
        this.init(new Inventory(), MAX_HEALTH, MAX_LOAD_CAPACITY, MAX_MONEY, MAX_HUMANENESS, MAX_REPUTATION);
    }

    public void init(Inventory inventory, float health, float loadCapacity, float money, float humaneness,
            float reputation) {
        try {
            init(inventory, new Health(health), new LoadCapacity(loadCapacity), new Money(money),
                    new Humaneness(humaneness), new Reputation(reputation));
        } catch (ValueOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public void init(Inventory inventory, Health health, LoadCapacity loadCapacity, Money money, Humaneness humaneness, Reputation reputation) {
        super.init(inventory, health, loadCapacity);
        this.money = money;
        this.humaneness = humaneness;
        this.reputation = reputation;
    }

    @Override
    public float get(EntityAttributeType variableName) throws UnknownEnumNameException {
        switch (variableName) {
            case MONEY:
                return money.get();
            case HUMANENESS:
                return humaneness.get();
            case REPUTATION:
                return reputation.get();
            default:
                return super.get(variableName);
        }
    }

    @Override
    public float getAsPercentage(EntityAttributeType variableName) throws UnknownEnumNameException {
        switch (variableName) {
            case MONEY:
                return money.getAsPercentage();
            case HUMANENESS:
                return humaneness.getAsPercentage();
            case REPUTATION:
                return reputation.getAsPercentage();
            default:
                return super.getAsPercentage(variableName);
        }
    }

    @Override
    public boolean set(EntityAttributeType variableName, float value) throws UnknownEnumNameException {
        switch (variableName) {
            case MONEY:
                return money.set(value);
            case HUMANENESS:
                return humaneness.set(value);
            case REPUTATION:
                return reputation.set(value);
            default:
                return super.set(variableName, value);
        }
    }

    @Override
    public boolean plus(EntityAttributeType variableName, float amount) throws UnknownEnumNameException {
		switch (variableName) {
            case MONEY:
                return money.plus(amount);
            case HUMANENESS:
                return humaneness.plus(amount);
            case REPUTATION:
                return reputation.plus(amount);
            default:
                return super.plus(variableName, amount);
        }
    }

    @Override
    public boolean minus(EntityAttributeType variableName, float amount) throws UnknownEnumNameException {
        switch (variableName) {
            case MONEY:
                return money.minus(amount);
            case HUMANENESS:
                return humaneness.minus(amount);
            case REPUTATION:
                return reputation.minus(amount);
            default:
                return super.minus(variableName, amount);
        }
    }
    
    @Override
	public Term getTerm() {
		return TERM;
	}
    
    @Override
    public Element createXMLContents(Document ofDoc) {
    	Element main = super.createXMLContents(ofDoc);
    	ofDoc.renameNode(main, null, TERM.getText());
    	main.appendChild(this.money.createXMLContents(ofDoc));
    	main.appendChild(this.humaneness.createXMLContents(ofDoc));
    	main.appendChild(this.reputation.createXMLContents(ofDoc));
    	return main;
    }
    
}