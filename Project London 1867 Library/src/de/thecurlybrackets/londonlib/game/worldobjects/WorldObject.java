package de.thecurlybrackets.londonlib.game.worldobjects;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.LondonWorld;
import de.thecurlybrackets.londonlib.game.gui.Texture;
import de.thecurlybrackets.londonlib.parser.components.LondonMod;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

/**
 * A WorldObject is any part of a LondonWorld
 * allowing user-interaction
 * 
 * @author Tim Roethig
 * @see LondonWorld
 */
public abstract class WorldObject extends Identifiable {
	protected Name name = DEFAULT_NAME;
	protected Description description = DEFAULT_DESCRIPTION;
	protected Texture texture = null;
	
	/* defaults */
	/**
	 * Default Name of any WorldObject
	 */
	public static final Name DEFAULT_NAME = new Name();
	
	/**
	 * Default Description of any WorldObject
	 */
	public static final Description DEFAULT_DESCRIPTION = new Description("Mystery is amongst me");

	protected WorldObject(Label label) {
		super(label);
	}
	
	protected WorldObject(String label) {
		super(label);
	}
	
	public WorldObject(Label label, LondonPart parentPart) {
		super(label, parentPart);
	}
	
	public WorldObject(String label, LondonPart parentPart) {
		super(label, parentPart);
	}
	
	public WorldObject(Label label, LondonPart parentPart, Name name, Description description) {
		super(label, parentPart);
		this.name = name;
		this.description = description;
	}
	
	public WorldObject(String label, LondonPart parentPart, Name name, Description description) {
		super(label, parentPart);
		this.name = name;
		this.description = description;
	}
	
	public WorldObject(Label label, LondonMod parentMod) {
		super(label, parentMod);
	}
	
	public WorldObject(String label, LondonMod parentMod) {
		super(label, parentMod);
	}
	
	public WorldObject(Label label, LondonMod parentMod, Name name, Description description) {
		super(label, parentMod);
		this.name = name;
		this.description = description;
	}
	
	public WorldObject(String label, LondonMod parentMod, Name name, Description description) {
		super(label, parentMod);
		this.name = name;
		this.description = description;
	}
	
	public Name getName() {
		return this.name;
	}
	
	public void setName(Name name) {
		this.name = name;
	}
	
	public Description getDescription() {
		return this.description;
	}
	
	public void setDescription(Description description) {
		this.description = description;
	}
	
	/**
	 * Loads a {@link Texture}, named {@link #getLabel()},
	 * from the resource-directory of its owner
	 * @return the loaded Texture or {@code null} if it has no
	 * Texture
	 */
	public Texture loadItsTexture() {
		//TODO
		return this.texture;
	}
	
	/**
	 * Loads {@link Texture} from any
	 * {@link Texture#TEXTURE_EXTENSION}-file
	 * 
	 * @param fileToTexture
	 * @return the loaded Texture or {@code null} if it has no
	 * Texture
	 */
	public Texture loadSpecificTexture(File fileToTexture) {
		//TODO
		return this.texture;
	}
	
	@Override
	public Element createXMLContents(Document ofDoc) {
		Element main = ofDoc.createElement(this.getTerm().getText());
		//attribute "label"
		main.setAttribute(ATTRNAME_LABEL, this.id.getLabel().getText());
		//attribute "name"
		main.setAttribute(Name.ATTRNAME_XMLNAME, this.name.toString());
		//description-node
		Element description = this.description.createXMLContents(ofDoc);
		main.appendChild(description);
		
		return main;
	}
}
