package de.thecurlybrackets.londonlib.game.worldobjects;

/**
 * Any {@link Text} naming an object
 * @author Tim Roethig
 *
 */
public class Name extends Text {
	/**
	 * The maximum amount of characters a Name
	 * is allowed to have
	 */
	public static final byte MAX_CHARS = 50;
	
	/**
	 * The default String-name used in {@link #Name()}
	 */
	public static final String DEFAULT_NAME_STR = "Nameless";
	
	/* attribute-names */
	/**
	 * XML-Name of any Name
	 */
	public static final String ATTRNAME_XMLNAME = "name";
	
	/**
	 * Calls {@link #Name(String)} where
	 * {@code name=} {@link #DEFAULT_NAME_STR}
	 */
	public Name() {
		this(DEFAULT_NAME_STR);
	}
	
	/**
	 * Creates a new Name and checks whether it
	 * is not too long [0,{@link #MAX_CHARS}]
	 * @param name The potential text of this name
	 */
	public Name(String name) {
		super(name.length() <= MAX_CHARS ? name : null);
	}
	
}
