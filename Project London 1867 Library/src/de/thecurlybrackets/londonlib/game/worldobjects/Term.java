package de.thecurlybrackets.londonlib.game.worldobjects;

/**
 * A Term describes the referring kind
 * of an ID (e.g. Entity) and splits all
 * child-information with every dot (.)
 * in order to yield its depth
 * @author Tim Roethig
 *
 */
public class Term extends Label {
	
	/**
	 * The split contents
	 */
	protected String[] contents;
	
	/**
	 * The text with brackets -> (TEXT)
	 */
	protected String bracText;
	
	/**
	 * Separator incrementing the depth by one
	 */
	public static final String TERM_COMPONENT_SEPARATOR = ".";
	
	/**
	 * Merges all given Strings to a single String using the pattern<br><br>
	 * {@code contents[i] +} {@link #TERM_COMPONENT_SEPARATOR} {@code + contents[i+1]}<br>
	 * <b>for</b> {@code i} <b>in</b> {@code {0, contents.length}}
	 * @param contents Strings to be merged
	 * @return All String-s merged to a single, dot-separated String
	 */
	public static String mergeStringArrayToTermText(String... contents) {
		StringBuilder mergedContents = new StringBuilder();
		int counter = 1;
		//merging all contents with a dot separator
		for(var i : contents) {
			//do not allow dots as they separate contents
			if(isStringIllegal(i, false)) {
				throw new IllegalArgumentException("");
			}
			mergedContents.append(i);
			if(counter < contents.length) {
				mergedContents.append(TERM_COMPONENT_SEPARATOR);
			}
			counter++;
		}
		return mergedContents.toString();
	}
	
	/**
	 * Calls {@link #setText(String)} and
	 * extracts {@code text}'s contents
	 * @param text The text of this Term
	 */
	public Term(String text) {
		super(text);
		this.contents = text.split(".");
	}
	
	/**
	 * Merges {@code contents} using
	 * {@link #mergeStringArrayToTermText(String...)}
	 * @param contents
	 */
	public Term(String... contents) {
		super(mergeStringArrayToTermText(contents));
		this.contents = contents;
	}
	
	public Term(Term other, String... appendix) {
		super();
		this.text = other.text + TERM_COMPONENT_SEPARATOR + mergeStringArrayToTermText(appendix);
	}
	
	/**
	 * 
	 * @return The separated text
	 */
	public final String[] getContents() {
		return this.contents;
	}
	
	/**
	 * @return The text embraced with brackets
	 */
	public String getTextWithBrackets() {
		return this.bracText;
	}
	
	@Override
	protected void setText(String text) {
		super.setText(text);
		this.bracText = "(" + this.text + ")";
		this.contents = text.split(TERM_COMPONENT_SEPARATOR);
		return;
	}
	
	/**
	 * As there has to be at least one
	 * element in contents, the first element
	 * labels the overall category of an Object
	 * @return The first element in {@code contents}
	 */
	public String getMainGroup() {
		return this.contents[0];
	}
	
}
