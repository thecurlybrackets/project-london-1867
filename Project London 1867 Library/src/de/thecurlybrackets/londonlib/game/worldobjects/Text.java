package de.thecurlybrackets.londonlib.game.worldobjects;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Denotes any kind of text being
 * translatable
 * 
 * @author Tim Roethig
 *
 */
public class Text implements XMLContent {
	
	protected String text;
	
	/* node-names */
	/**
	 * XML-Node-Name of any Text-Element
	 */
	public static final String NODENAME_XMLNAME = Text.class.getSimpleName();
	
	/**
	 * 
	 * @param elem The to-construct Text as XML-Element
	 * @return The constructed Text
	 */
	public static Text constructFromXMLContents(Element elem) {
		return new Text(elem.getTextContent());
	}
	
	/**
	 * Creates empty Text
	 */
	public Text() {
		this.text = "";
	}
	
	/**
	 * Calls {@link #setText(String)}
	 * @param text
	 */
	public Text(String text) {
		this.setText(text);
	}
	
	/**
	 * Assigns this' text
	 * with {@code text}
	 * @param text new text
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * 
	 * @return this text as {@link String}
	 */
	public String getText() {
		return this.text;
	}
	
	@Override
	public String toString() {
		return this.text;
	}

	@Override
	public Element createXMLContents(Document ofDoc) {
		Element main = ofDoc.createElement(NODENAME_XMLNAME);
		main.setTextContent(this.text);
		return main;
	}
	
}
