package de.thecurlybrackets.londonlib.game.worldobjects.props;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.worldobjects.Description;
import de.thecurlybrackets.londonlib.game.worldobjects.ID;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.Name;
import de.thecurlybrackets.londonlib.game.worldobjects.SingleParented;
import de.thecurlybrackets.londonlib.game.worldobjects.Term;
import de.thecurlybrackets.londonlib.game.worldobjects.WorldObject;
import de.thecurlybrackets.londonlib.game.worldobjects.places.Place;
import de.thecurlybrackets.londonlib.parser.components.Component;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

public abstract class Prop extends WorldObject implements Component<LondonPart>, SingleParented<Place> {
	
	protected LondonPart parentPart;
	protected Place parentPlace;
	
	public static final Term TERM = new Term(Prop.class.getSimpleName());
	
    public Prop(Label label, LondonPart parentPart) {
		super(label, parentPart);
	}
    
    public Prop(Label label, LondonPart parentPart, Name name, Description description) {
        super(label, parentPart, name, description);
    }
    
    @Override
    public Term getTerm() {
    	return TERM;
    }
    
    @Override
	public void bindTo(Label label, LondonPart destination) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void moveTo(LondonPart destination) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void rename(Label label) {
		//TODO
	}
    
    @Override
    public Element createXMLContents(Document ofDoc) {
    	Element main = super.createXMLContents(ofDoc);
    	
    	return main;
    }

	@Override
	public LondonPart getParentComponentCollection() {
		return this.parentPart;
	}

	@Override
	public ID getChildKey() {
		return this.getID();
	}

	@Override
	public void setParent(Place parent) {
		this.parentPlace = parent;
	}

	@Override
	public Place getParent() {
		return this.parentPlace;
	}
    
}