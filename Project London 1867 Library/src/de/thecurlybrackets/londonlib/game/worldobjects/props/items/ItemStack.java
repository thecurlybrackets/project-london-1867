package de.thecurlybrackets.londonlib.game.worldobjects.props.items;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.worldobjects.ID;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.XMLContent;
import de.thecurlybrackets.londonlib.game.worldobjects.props.Prop;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

public final class ItemStack implements XMLContent {

    private int maxSize;
    private int currSize = 0;
    private Item itemType;
    private Item current;
    
    /* node-names */
    public static final String NODENAME_ITEMS = "Items";
    
    /* attribute-names */
    public static final String ATTRNAME_MAX_SIZE = "maxSize";
    public static final String ATTRNAME_CURR_SIZE = "currSize";
    public static final String ATTRNAME_ITEM_LABEL = "itemLabel";
    
    public static ItemStack constructFromXMLContents(Element node, LondonPart londonPart) {
		Prop possibleItem = londonPart.getProp(new Label(node.getAttribute(ATTRNAME_ITEM_LABEL)));
		Item itemType = null;
		if(possibleItem instanceof Item) {
			itemType = (Item)possibleItem;
		} else {
			//TODO
		}
		return new ItemStack(itemType,
										Integer.parseUnsignedInt((node.getAttribute(ATTRNAME_MAX_SIZE))),
										Integer.parseUnsignedInt(node.getAttribute(ATTRNAME_CURR_SIZE)));
	}
    
    public ItemStack(Item itemType, int maxSize) {
        if(maxSize < 1) {
            throw new IndexOutOfBoundsException("The max size has to be at least one");
        }
        this.itemType = itemType;
        this.maxSize = maxSize;
    }
    
    public ItemStack(Item itemType, int maxSize, int currSize) {
        this(itemType, maxSize);
        this.setCurrentSize(maxSize);
    }
    
    public ID getItemTypeID() {
    	return this.itemType.getID();
    }
    
    public void setCurrentSize(int currSize) {
    	if(currSize > this.maxSize) {
    		throw new IndexOutOfBoundsException("");
    	}
    	this.currSize = currSize;
    	return;
    }
    
    public boolean pushNew() {
        if(this.maxSize == this.currSize) {
            return false;
        }
        this.currSize++;
        return true;
    }
    
    public Item peek() {
    	return this.current;
    }
    
    public Item poll() {
        if(this.currSize > 0) {
        	this.currSize--;
        	return this.itemType;
        }
        return null;
    }

    public int getCurrentSize() {
        return this.currSize;
    }

	@Override
	public Element createXMLContents(Document ofDoc) {
		Element main = ofDoc.createElement(this.getClass().getSimpleName());
		main.setAttribute(ATTRNAME_MAX_SIZE, String.valueOf(this.maxSize));
		main.setAttribute(ATTRNAME_CURR_SIZE, String.valueOf(this.currSize));
		main.setAttribute(ATTRNAME_ITEM_LABEL, this.itemType.getTerm().getText());
		//adding "Items" and its belongings
		Element items = ofDoc.createElement(NODENAME_ITEMS);
		main.appendChild(items);
		
		return main;
	}
}