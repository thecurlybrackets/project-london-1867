package de.thecurlybrackets.londonlib.game.worldobjects.props.items;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.worldobjects.Description;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.Name;
import de.thecurlybrackets.londonlib.game.worldobjects.Term;
import de.thecurlybrackets.londonlib.game.worldobjects.attributes.props.Weight;
import de.thecurlybrackets.londonlib.game.worldobjects.props.Prop;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

public abstract class Item extends Prop {

    public static final int DEFAULT_STACKABILITY = 1;
    private Weight weight;
    
    public static final Term TERM = new Term(Item.class.getSimpleName());
    
    public static final String ATTRNAME_WEIGHT = "weight";
    
    public Item(Label label, LondonPart parentPart) {
        super(label, parentPart);
    }

    public Item(Label label, LondonPart parentPart, Name name, Description description) {
        super(label, parentPart, name, description);
    }

    public float getWeight() {
        return weight.get();
    }

    public int getStackability() {
        return DEFAULT_STACKABILITY;
    }
    
    @Override
    public Element createXMLContents(Document ofDoc) {
    	Element main = super.createXMLContents(ofDoc);
    	//renaming to 'Item'
    	ofDoc.renameNode(main, null, this.getTerm().getText());
    	//adding attribute "weight"
    	main.setAttribute(ATTRNAME_WEIGHT, String.valueOf(this.weight.get()));
    	
    	return main;
    }
    
}
