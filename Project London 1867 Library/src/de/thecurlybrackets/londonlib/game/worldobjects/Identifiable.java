package de.thecurlybrackets.londonlib.game.worldobjects;

import de.thecurlybrackets.londonlib.parser.components.LondonMod;
import de.thecurlybrackets.londonlib.parser.components.LondonPart;

/**
 * Any type which has to be identifiable via
 * an ID
 * @author Tim Roethig
 * @see ID
 */
public abstract class Identifiable implements Labeled, XMLContent {
	/**
	 * @see #getID()
	 */
	protected ID id;
	
	/**
	 * Initializes this' ID
	 * using {@link ID#ID(Term, Label)}
	 * where {@code Term} is {@link #getTerm()}.
	 * @param label Label
	 */
	protected Identifiable(Label label) {
		id = new ID(this.getTerm(), label);
	}
	
	/**
	 * Synonym
	 * @see #Identifiable(Label)
	 * @param label
	 */
	protected Identifiable(String label) {
		this(new Label(label));
	}
	
	/**
	 * Initializes this' ID
	 * using {@link ID#ID(Term, LondonMod, Label)}
	 * @param term {@code Term}
	 * @param label {@code Label}
	 * @param parentMod {@code LondonMod}
	 */
	protected Identifiable(Term term, Label label, LondonMod parentMod) {
		this.id = new ID(term, parentMod, label);
	}
	
	/**
	 * Synonym
	 * @see #Identifiable(Term, Label, LondonMod)
	 * @param term {@code Term}
	 * @param label {@code Label}
	 * @param parentMod {@code LondonMod}
	 */
	protected Identifiable(Term term, String label, LondonMod parentMod) {
		this.id = new ID(term, parentMod, label);
	}
	
	/**
	 * Initializes this' ID
	 * using {@link ID#ID(Term, LondonPart, Label)}
	 * @param term {@code Term}
	 * @param label {@code Label}
	 * @param parentPart {@code LondonPart}
	 */
	protected Identifiable(Term term, Label label, LondonPart parentPart) {
		this.id = new ID(term, parentPart, label);
	}
	
	/**
	 * Synonym
	 * @see #Identifiable(Term, Label, LondonPart)
	 * @param term {@code Term}
	 * @param label {@code Label}
	 * @param parentMod {@code LondonMod}
	 */
	protected Identifiable(Term term, String label, LondonPart parentPart) {
		this.id = new ID(term, parentPart, label);
	}
	
	/**
	 * Like {@link #Identifiable(Term, Label, LondonMod)}
	 * but determines the {@code Term} via {@link #getTerm()}
	 * @param label {@code Label}
	 * @param parentMod {@code LondonMod}
	 */
	public Identifiable(Label label, LondonMod parentMod) {
		this.id = new ID(this.getTerm(), parentMod, label);
	}
	
	/**
	 * Synonym
	 * @see #Identifiable(Label, LondonMod)
	 * @param label
	 * @param parentMod
	 */
	public Identifiable(String label, LondonMod parentMod) {
		this.id = new ID(this.getTerm(), parentMod, label);
	}
	
	/**
	 * Like {@link #Identifiable(Term, Label, LondonPart)}
	 * but determines the {@code Term} via {@link #getTerm()}
	 * @param label {@code Label}
	 * @param parentPart {@code LondonMod}
	 */
	public Identifiable(Label label, LondonPart parentPart) {
		this.id = new ID(this.getTerm(), parentPart, label);
	}
	
	/**
	 * Synonym
	 * @see #Identifiable(Label, LondonPart)
	 * @param label
	 * @param parentPart
	 */
	public Identifiable(String label, LondonPart parentPart) {
		this.id = new ID(this.getTerm(), parentPart, label);
	}
	
	/**
	 * The Term of the Identifiable describing
	 * its purpose
	 * @return The Term
	 */
	public abstract Term getTerm();
	
	/**
	 * The Identifier of the owner possessing
	 * this Identifiable
	 * @return The owner's identifier
	 */
	public Label getOwnerLabel() {
		return this.id.getOwnerLabel();
	}
	
	/**
	 * Describes the kind of owner
	 * @return The owner's suffix
	 */
	public Label getOwnerSuffix() {
		return this.id.getOwnerSuffix();
	}
	
	@Override
	public Label getLabel() {
		return this.id.getLabel();
	}
	
	/**
	 * Wraps {@link ID#toString()}
	 */
	@Override
	public String toString() {
		return this.id.toString();
	}
	
	/**
	 * 
	 * @return The ID of this Identifiable
	 */
	public ID getID() {
		return this.id;
	}
	
}
