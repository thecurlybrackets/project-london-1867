package de.thecurlybrackets.londonlib.game.worldobjects;

/**
 * A Label is any sort of text
 * which shall neither be null nor
 * empty nor contain forbidden chars.
 * That fulfillment of this rule-set can be checked with
 * {@link #isStringIllegal(String, boolean)}
 * It is mainly used
 * to express parts of an ID
 * 
 * @author Tim Roethig
 * @see ID
 */
public class Label {
	protected String text;
	
	/**
	 * Legal {@link String}s only contain the letters
	 * a-z, A-Z, 0-9 as well as it might be allowed to contain dots.
	 * It shall neither be empty nor {@code null}
	 * @param str The String to check on
	 * @param allowDot {@code true} in order to allow dots
	 * @return {@code true} on an unfulfilled rule-set
	 */
	public static boolean isStringIllegal(String str, boolean allowDot){
		if(str == null) {
			return true;
		} else if(str.isEmpty()) {
			return true;
		} else {
			for(int i = 0; i < str.length(); i++) {
				if((str.charAt(i) >= 'a') && (str.charAt(i) <= 'z')) {
					continue;
				} else if((str.charAt(i) >= 'A') && (str.charAt(i) <= 'Z')) {
					continue;
				} else if((str.charAt(i) >= '0') && (str.charAt(i) <= '9')) {
					continue;
				} else if(str.charAt(i) == '.') {
					if(allowDot) {
						continue;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.text;
	}
	
	@Override
	public int hashCode() {
		return this.text.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
        if (obj == null) {
        	return false;
        }
        
        if (this.getClass() != obj.getClass()) {
        	return false;
        }
        Label other = (Label)obj;
        return other.text.equals(this.text);
	}
	
	protected Label() {
		
	}
	
	/**
	 * Calls {@link #setText(String)}
	 * @param text The potential text this Label
	 */
	public Label(String text) {
		this.setText(text);
	}
	
	/**
	 * Sets its text to {@code text} if it passes
	 * {@link #isStringIllegal(String, boolean)}<br>
	 * where {@code allowDot = true}
	 * @param text The potential text this Label
	 * @throws IllegalArgumentException if {@link #isStringIllegal(String, boolean)}
	 * returns {@code true}
	 */
	protected void setText(String text) throws IllegalArgumentException {
		if(Label.isStringIllegal(text, true)) {
			throw new IllegalArgumentException("");
		}
		this.text = text;
	}
	
	/**
	 * 
	 * @return the text of this Label
	 */
	public String getText() {
		return this.text;
	}
	
}
