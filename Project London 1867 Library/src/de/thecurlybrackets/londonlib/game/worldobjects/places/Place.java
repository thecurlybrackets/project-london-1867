package de.thecurlybrackets.londonlib.game.worldobjects.places;

import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.worldobjects.Description;
import de.thecurlybrackets.londonlib.game.worldobjects.ID;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.Name;
import de.thecurlybrackets.londonlib.game.worldobjects.Parent;
import de.thecurlybrackets.londonlib.game.worldobjects.Term;
import de.thecurlybrackets.londonlib.game.worldobjects.VastlyParented;
import de.thecurlybrackets.londonlib.game.worldobjects.WorldObject;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.Entity;
import de.thecurlybrackets.londonlib.game.worldobjects.props.Prop;
import de.thecurlybrackets.londonlib.parser.components.Component;
import de.thecurlybrackets.londonlib.parser.components.LondonMod;

/**
 * Represents a node inside of a network of other Places.
 * Each contains {@link Entity Entities} and {@link Prop Props}
 * which shall make a Place vivid and interactive
 * @author Tim Roethig
 *
 */
public class Place extends WorldObject implements Parent<Place>, Component<LondonMod>, VastlyParented<Place> {
	protected HashMap<ID, Place> parentPlaces = new HashMap<>();
	protected HashMap<ID, Place> childrenPlaces = new HashMap<>();
	
	protected LondonMod parentMod;
	protected HashMap<ID, Entity> entities = new HashMap<>();
	protected HashMap<ID, Prop> props = new HashMap<>();
	
	public static final Term TERM = new Term(Place.class.getSimpleName());
	
	/* node-names */
	
	public static final String NODENAME_PROP_REFERENCES 	= "PropReferences";
	public static final String NODENAME_PROP_REFERENCE 		= "PropReference";
	public static final String NODENAME_ENTITY_REFERENCES 	= "EntityReferences";
	public static final String NODENAME_ENTITY_REFERENCE 	= "EntityReference";
	
	/**
	 * Extracts all Place-data from
	 * an XML-Element and makes it a Place again
	 * @param elem the XML-contents
	 * @param parentMod the parent of this {@link Place}
	 * @return the extracted data
	 */
	public Place constructFromXMLContents(Element elem, LondonMod parentMod) {
		var xmlLabel = elem.getAttribute(ATTRNAME_LABEL);
		var xmlName = elem.getAttribute(Name.ATTRNAME_XMLNAME);
		
		var xmlDescription = ((Element)elem.getElementsByTagName(Description.NODENAME_XMLNAME)).getTextContent();
		var xmlProps = ((Element)elem.getElementsByTagName(NODENAME_PROP_REFERENCES)).getChildNodes();
		var xmlEntities = ((Element)elem.getElementsByTagName(NODENAME_ENTITY_REFERENCES)).getChildNodes();
		Place result = new Place(xmlLabel, parentMod, new Name(xmlName), new Description(xmlDescription));
		//adding props
		for(int i = 0; i < xmlProps.getLength(); i++) {
			result.placeElementHere(new ID(xmlProps.item(i).getTextContent()));
		}
		//adding entities
		for(int i = 0; i < xmlEntities.getLength(); i++) {
			result.placeElementHere(new ID(xmlProps.item(i).getTextContent()));
		}
		
		return result;
	}
	
	public Place(Label label, LondonMod parentMod) {
		super(label, parentMod);
		this.parentMod = parentMod;
		this.parentMod.addElement(this);
	}
	
	public Place(String label, LondonMod parentMod) {
		super(label, parentMod);
		this.parentMod = parentMod;
		this.parentMod.addElement(this);
	}
	
	public Place(Label label, LondonMod parentMod, Name name, Description description) {
		super(label, parentMod, name, description);
	}
	
	public Place(String label, LondonMod parentMod, Name name, Description description) {
		super(label, parentMod, name, description);
	}
	
	/**
	 * Places any {@link Prop} or {@link Entity}
	 * inside this Place via an {@link ID} in order
	 * to load it from the {@link de.thecurlybrackets.londonlib.parser.components.SessionPool SessionPool}
	 * @param which The ID of the Element to be loaded from the pool and thereinafter placed
	 * inside this Place
	 */
	public void placeElementHere(ID which) {
		if(which.getTerm().getMainGroup().equals(Prop.TERM.getMainGroup())) {
			var ref = this.parentMod.getSessionPool().getProp(which.getOwnerLabel(), which.getLabel());
			ref.setParent((this));
			this.props.put(which, ref);
		} else if(which.getTerm().getMainGroup().equals(Entity.TERM.getMainGroup())) {
			var ref = this.parentMod.getSessionPool().getEntity(which.getOwnerLabel(), which.getLabel());
			ref.setParent(this);
			this.entities.put(which, ref);
		} else {
			//TODO
		}
		return;
	}
	
	@Override
	public Term getTerm() {
		return TERM;
	}
	
	@Override
	public Element createXMLContents(Document ofDoc) {
		Element main = ofDoc.createElement(TERM.getText());
		//attribute "label"
		main.setAttribute(ATTRNAME_LABEL, this.id.getLabel().getText());
		//attribute "name"
		main.setAttribute(Name.ATTRNAME_XMLNAME, this.name.toString());
		//description-node
		Element description = this.description.createXMLContents(ofDoc);
		main.appendChild(description);
		//the props
		Element props = ofDoc.createElement(NODENAME_PROP_REFERENCES);
		for(var i : this.props.values()) {
			//the current entity-reference
			Element curr = ofDoc.createElement(NODENAME_ENTITY_REFERENCE);
			curr.setTextContent(i.getID().toString());
			props.appendChild(curr);
		}
		main.appendChild(props);
		//the entities
		Element entities = ofDoc.createElement(NODENAME_ENTITY_REFERENCES);
		for(var i : this.entities.values()) {
			//the current entity-reference
			Element curr = ofDoc.createElement(NODENAME_ENTITY_REFERENCE);
			curr.setTextContent(i.getID().toString());
			entities.appendChild(curr);
		}
		main.appendChild(entities);
		
		return main;
	}

	@Override
	public void bindTo(Label label, LondonMod destination) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void moveTo(LondonMod destination) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void rename(Label label) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public LondonMod getParentComponentCollection() {
		return this.parentMod;
	}

	@Override
	public ID getChildKey() {
		return this.getID();
	}

	@Override
	public void addParent(Place parent) {
		this.parentPlaces.put(parent.getChildKey(), parent);
	}

	@Override
	public HashMap<ID, Place> getParents() {
		return this.parentPlaces;
	}

	@Override
	public ID getParentKey() {
		return this.getID();
	}

	@Override
	public HashMap<ID, Place> getChildren() {
		return this.childrenPlaces;
	}

	@Override
	public void addChild(Place child) {
		this.childrenPlaces.put(child.getChildKey(), child);
		return;
	}

	@Override
	public int getParentCount() {
		return this.parentPlaces.size();
	}
}
