package de.thecurlybrackets.londonlib.game.worldobjects;

import org.w3c.dom.Element;

/**
 * Structure containing extracted data
 * of a {@link WorldObject}
 * @author Tim Roethig
 *
 */
public class WorldObjectXMLExtract implements Labeled {
	
	protected Label label;
	protected Name name;
	protected Description description;
	
	/**
	 * Extracts all {@link WorldObject}-data from
	 * an XML-Element and stores it as its XMLExtract
	 * @param elem the XML-contents
	 * @return the extracted data
	 */
	public static WorldObjectXMLExtract constructFromXMLContents(Element elem) {
		
		var xmlLabel = elem.getAttribute(WorldObject.ATTRNAME_LABEL);
		var xmlName = elem.getAttribute(Name.ATTRNAME_XMLNAME);
		var xmlDescription = (Element)(elem.getElementsByTagName(Description.NODENAME_XMLNAME).item(0));
		
		return new WorldObjectXMLExtract(new Label(xmlLabel), new Name(xmlName), Description.constructFromXMLContents(xmlDescription));
	}
	
	/**
	 * Merges all extracted data at once
	 * @param label Extracted Label
	 * @param name Extracted Name
	 * @param description Extracted Description
	 */
	public WorldObjectXMLExtract(Label label, Name name, Description description) {
		this.label = label;
		this.name = name;
		this.description = description;
	}
	
	@Override
	public Label getLabel() {
		return this.label;
	}
	
	public Name getName() {
		return this.name;
	}
	
	public Description getDescription() {
		return this.description;
	}
	
}
