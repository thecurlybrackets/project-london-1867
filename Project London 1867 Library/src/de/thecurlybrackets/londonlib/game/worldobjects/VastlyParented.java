package de.thecurlybrackets.londonlib.game.worldobjects;

import java.util.Map;

/**
 * Denotes a {@link Child} having multiple {@link Parent}
 * @author Tim Roethig
 *
 * @param <TParentType> type of the {@link Parent}
 */
public interface VastlyParented<TParentType extends Parent<?>> extends Child {
	
	/**
	 * Adds the given {@code parent} to this 
	 * child
	 * @param parent the parent to be added
	 */
	public void addParent(TParentType parent);
	
	/**
	 * Removes the given {@code parent} from this 
	 * child 
	 * @param parent the parent to be removed
	 */
	public default void removeParent(TParentType parent) {
		this.getParents().remove(parent.getParentKey());
	}
	
	/**
	 * 
	 * @return all parents of this child
	 */
	public Map<?, TParentType> getParents();
	
	/**
	 * Tells the amount of parents this instance depends on
	 * @return the size of the parent collection
	 */
	public int getParentCount();
	
}
