package de.thecurlybrackets.londonlib.game.worldobjects;

/**
 * Provides messages and message-generation
 * for Exception-s
 * 
 * @author Tim Roethig
 *
 */
public abstract class ExceptionMessages {
	
	public static String makeIDParsingErrorMessage(String parsedID) {
		return "The given ID (" + parsedID + ") has an invalid format!";
	}
	
}
