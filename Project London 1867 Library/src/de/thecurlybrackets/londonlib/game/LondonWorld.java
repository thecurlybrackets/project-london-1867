package de.thecurlybrackets.londonlib.game;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.thecurlybrackets.londonlib.game.worldobjects.ID;
import de.thecurlybrackets.londonlib.game.worldobjects.Label;
import de.thecurlybrackets.londonlib.game.worldobjects.Labeled;
import de.thecurlybrackets.londonlib.game.worldobjects.Name;
import de.thecurlybrackets.londonlib.game.worldobjects.WorldObject;
import de.thecurlybrackets.londonlib.game.worldobjects.entities.Player;
import de.thecurlybrackets.londonlib.parser.StorableXMLData;
import de.thecurlybrackets.londonlib.parser.XMLDataFormat;
import de.thecurlybrackets.londonlib.parser.components.ComponentCollectionLoader;
import de.thecurlybrackets.londonlib.parser.components.LondonMod;
import de.thecurlybrackets.londonlib.parser.components.SessionPool;
import de.thecurlybrackets.londonlib.parser.exceptions.SaveObjectToFileFailureException;
import de.thecurlybrackets.londonlib.parser.translation.TranslationInvoker;

/**
 * Loads {@link LondonMod LondonMods} in order to
 * acquire all necessary {@link WorldObject WorldObjects}
 * of which selected (or maybe all)
 * {@link de.thecurlybrackets.londonlib.game.worldobjects.places.Place Places}
 * are connected to a {@link PlaceConnection}.
 * Additionally, it defines the initial {@link Player}.<br>
 * All changes are saved to a {@link #LONDONVAULT_EXTENSION .londonvault}-file
 * via {@link #saveAsLondonvault(String)}
 * @author Tim Roethig
 */
public class LondonWorld extends StorableXMLData implements Labeled, TranslationInvoker,
								ComponentCollectionLoader<LondonMod> {
	protected SessionPool sessionPool;
	protected HashMap<Label, LondonMod> mods = new HashMap<>();
	protected ArrayList<ID> changes = new ArrayList<>();
	protected Label label;
	protected Name name = DEFAULT_NAME;
	protected ArrayList<PlaceConnection> placeConnections = new ArrayList<>();
	protected Player player;
	
	/* defaults */
	/**
	 * The default {@link Name} of a LondonWorld
	 */
	public static final Name DEFAULT_NAME = new Name("A world in Paradise");
	
	/* filesystem */
	/**
	 * The file-extension of any LondonWorld
	 */
	public static final String WORLD_EXTENSION 			= ".londonworld";
	
	/**
	 * The file-extension of any save-game
	 */
	public static final String LONDONVAULT_EXTENSION 	= ".londonvault";
	
	/**
	 * The relative directory of all {@link #WORLD_EXTENSION .londonworld}-files
	 */
	public static final String WORLDS_DIR 				= "worlds/";
	
	/**
	 * The relative directory of all {@link #LONDONVAULT_EXTENSION .londonvault}-files
	 */
	public static final String LONDONVAULT_DIR 			= "savegames/";
	
	/* node-names */
	/**
	 * XML-Node-Name of all {@link PlaceConnection PlaceConnections}
	 */
	public static final String NODENAME_PLACECONNECTIONS = "PlaceConnections";
	
	/* attribute-names */
	/**
	 * XML-Attribute-Name for all {@link LondonMod LondonMods}
	 */
	public static final String ATTRNAME_MODS 	= "mods";
	
	/**
	 * Loads an existing or creates a new
	 * {@link #WORLD_EXTENSION .londonworld}-file
	 * in {@link #WORLDS_DIR} having the name
	 * {@code worldLabel}
	 * @param worldLabel Label of this world
	 * @param sessionPool SessionPool this world is using
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public LondonWorld(Label worldLabel, SessionPool sessionPool) throws IllegalArgumentException, IOException, ParserConfigurationException {
		super(new File(WORLDS_DIR + worldLabel.toString() + WORLD_EXTENSION), new XMLDataFormat());
		this.sessionPool = sessionPool;
		this.label = worldLabel;
		this.player = new Player(this);
	}
	
	/**
	 * Synonym
	 * @see #LondonWorld(Label, SessionPool)
	 * @param worldLabel Label of this world
	 * @param sessionPool SessionPool this world is using
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public LondonWorld(String worldLabel, SessionPool sessionPool) throws IllegalArgumentException, IOException, ParserConfigurationException {
		this(new Label(worldLabel), sessionPool);
	}
	
	public Name getName() {
		return this.name;
	}
	
	@Override
	public Label getLabel() {
		return this.label;
	}
	
	/**
	 * 
	 * @return The current {@link Player} of this world
	 */
	public Player getPlayer() {
		return this.player;
	}
	
	@Override
	public String getFileExtension() {
		return WORLD_EXTENSION;
	}

	/**
	 * 
	 * @param sgName The save-game's name without extension
	 */
	public void saveAsLondonvault(String sgName) {
		//TODO
	}
	
	@Override
	public void saveContents(File file) throws SaveObjectToFileFailureException {
		Document doc = this.formattedData.builder.newDocument();
		Element root = doc.createElement(this.getClass().getSimpleName());
		//id="???"
		root.setAttribute(ATTRNAME_LABEL, this.label.getText());
		//separate codeblock for creating the mod-list
		{
			//in order to avoid a potential reallocation
			int modsLength = 0;
			for(var i : this.mods.entrySet()) {
				modsLength += i.getValue().getLabel().getText().length() + 1; //ignore the additional 1 at the end
			}
			StringBuilder mods = new StringBuilder(modsLength);
			modsLength = 1; //reuse for counting
			for(var i : this.mods.entrySet()) {
				mods.append(i.getValue().getLabel());
				if(modsLength < this.mods.size()) {
					mods.append(LondonMod.LIST_ATTRIBUTE_DELIMITER);
				}
				modsLength++;
			}
			
			//mods="???|???|..."
			root.setAttribute(ATTRNAME_MODS, mods.toString());
		}
		Element placeCnnections = doc.createElement(NODENAME_PLACECONNECTIONS);
		//adding connections
		for(var i : this.placeConnections) {
			placeCnnections.appendChild(i.createXMLContents(doc));
		}
		
		root.appendChild(placeCnnections);
		//add the player as last element in tree
		root.appendChild(this.player.createXMLContents(doc));
		doc.appendChild(root);
		
		//writing to file
		{
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);
			try {
				this.getFormattedData().transformer.transform(source, result);
			} catch (TransformerException e) {
				//relabel exception
				throw new SaveObjectToFileFailureException(this, file, e.getMessage());
			}
		}
		return;
	}
	
	@Override
	public void loadContents(File file) {
		if(!file.getName().endsWith(this.getFileExtension())) {
			throw new IllegalArgumentException("");
		}
		
	}

	@Override
	public void translateTo(Locale locale) {
		//redirecting translation-request
		for(var mod : this.mods.entrySet()) {
			mod.getValue().translateTo(locale);
		}
	}

	@Override
	public void attachComponentCollection(LondonMod mod) {
		mod.setParentLondonWorld(this);
		this.mods.put(mod.getLabel(), mod);
		return;
	}

	@Override
	public void detachComponentCollection(LondonMod mod) {
		mod.setParentLondonWorld(this);
		this.mods.remove(mod.getLabel());
		return;
	}
	
	@Override
	public SessionPool getSessionPool() {
		return this.sessionPool;
	}
	
	/**
	 * Sets all child-LindonMod-s' parent LondonWorld
	 * to {@code null} and detaches itself from its SessionPool
	 */
	public void unload() {
		for(var i : this.mods.entrySet()) {
			i.getValue().setParentLondonWorld(null);
		}
		this.sessionPool = null;
		return;
	}

	@Override
	public Object getParentKey() {
		return this.getLabel();
	}
	
	/**
	 * Synonym
	 * @see #attachComponentCollection(LondonMod)
	 */
	@Override
	public void addChild(LondonMod child) {
		this.attachComponentCollection(child);
		return;
	}
	
	/**
	 * Synonym
	 * @see #detachComponentCollection(LondonMod)
	 */
	@Override
	public void removeChild(LondonMod child) {
		this.attachComponentCollection(child);
		return;
	}

	@Override
	public HashMap<Label, LondonMod> getChildren() {
		return this.mods;
	}
	
}
