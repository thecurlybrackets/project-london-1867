package de.thecurlybrackets.londonlib.game.gui;

import java.io.File;
import java.io.IOException;

import de.thecurlybrackets.londonlib.parser.StorableXMLData;
import de.thecurlybrackets.londonlib.parser.XMLDataFormat;
import de.thecurlybrackets.londonlib.parser.exceptions.SaveObjectToFileFailureException;

/**
 * Defines the look
 * of any WorldObject using
 * either ASCII-Still-Life (one frame) or
 * ASCII-Life (animated ASCII-Art) (multiple frames).
 * It uses a TextureFrame for each frame.
 * 
 * @author Tim Roethig
 * @see TextureFrame
 * @see de.thecurlybrackets.londonlib.game.worldobjects.WorldObject
 */
public class Texture extends StorableXMLData {
	protected TextureFrame[] frames = null;
	
	public Texture(File loadFile, XMLDataFormat formattedData) throws IllegalArgumentException, IOException {
		super(loadFile, formattedData);
		// TODO Auto-generated constructor stub
	}

	/**
	 * The file-extension of any Texture
	 */
	public static final String TEXTURE_EXTENSION = ".asciitexture";

	@Override
	public String getFileExtension() {
		return TEXTURE_EXTENSION;
	}

	@Override
	public void saveContents(File file) throws SaveObjectToFileFailureException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadContents(File file) throws IllegalArgumentException, IOException {
		// TODO Auto-generated method stub
		
	}
}
